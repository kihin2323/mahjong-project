import express from "express";
import GameController from './game.controller';
import GameService from './game.service';
import Knex from "knex";
import { knex } from './main';

describe("game controller test suite", () => {
    let get: any;
    let post: any;
    let gameController: GameController
    let gameService: GameService = { knex, get, post } as any
    let req: express.Request
    let res: express.Response

    beforeEach(async () => {
        gameService = new GameService({} as Knex)

        jest.spyOn(gameService, "readGame")
            .mockImplementation(async () => [{
                "created_at": 2020 - 10 - 20, "game_id": 60, "id": 246,
                "name": "player1", "score": 0, "updated_at": 2020 - 10 - 20
            }, {
                "created_at": 2020 - 10 - 20,
                "game_id": 60, "id": 247, "name": "player2", "score": 0, "updated_at": 2020 - 10 - 20
            },
            {
                "created_at": 2020 - 10 - 20, "game_id": 60, "id": 248, "name": "player3", "score": 0,
                "updated_at": 2020 - 10 - 20
            }, {
                "created_at": 2020 - 10 - 20, "game_id": 60, "id": 249,
                "name": "player4", "score": 0, "updated_at": 2020 - 10 - 20
            }])

        jest.spyOn(gameService, "startNewGame")
            .mockImplementation(async () => [{
                fanso: 10,
                money: 96,
                p1name: "newplayer1",
                p2name: "newplayer2",
                p3name: "newplayer3",
                p4name: "newplayer4"
            }])

        gameController = new GameController(gameService)

        req = {} as express.Request
        res = {
            query: jest.fn(),
            json: jest.fn(),
            status: jest.fn(() => {
                return res
            }),
        } as any
    })

    it("should enable the game id to be get", async () => {
        let query = req.query.id
        console.log(query)
        await gameController.get(req, res)
        expect(gameService.readGame).toBeCalled();
        
    })

    it("should be able to start a new game with new players and return the game id", async () => {
        req.body = {
            fanso: 10,
            money: 96,
            p1name: "newplayer1",
            p2name: "newplayer2",
            p3name: "newplayer3",
            p4name: "newplayer4"
        } as any
        await gameController.post(req, res)
        expect(res.json).toBeCalledWith({
            "game_id": [{
                "fanso": 10,
                "money": 96,
                "p1name": "newplayer1",
                "p2name": "newplayer2",
                "p3name": "newplayer3",
                "p4name": "newplayer4",
            }]
        }
        )
    })
})

