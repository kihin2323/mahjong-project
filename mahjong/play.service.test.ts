import PlayService from './play.service';
import Knex from "knex";
import knexConfigs from "./knexfile";

describe("play service test suite", () => {
    let knex: Knex;
    let playService: PlayService;

    beforeEach(async () => {
        knex = Knex(knexConfigs.active)
        await knex("round_score").del()
        await knex("player").del()
        await knex("game").del()

        await knex("round_score").insert({

        })
        
        let [game_id] = await knex("game").insert({
            fanso: 8,
            money: 32,
            user_id: 1
        }).returning("id")

        await knex("player").insert({
            name: "player1",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player2",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player3",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player4",
            score: 0,
            game_id
        });
        playService = new PlayService(knex)
    })

    afterAll(() => {
        knex.destroy();
    })

    it("should check game record, return round score and player names", async () => {
        const gameRecord = await playService.checkRecord(1);
        expect(gameRecord).toBeDefined;
    })

    it("should check", async () => {
        let updateResult = 
        await playService.updateScore({
            player: "player1",
            eatingWay: "breadSelfTouching",
            targetPlayer: "player3",
            points: 9,
            gameID: 1,
            userID_0: 1,
            userID_1: 2,
            userID_2: 3,
            userID_3: 4,
            money: 32
        }
        )
        expect(updateResult).toBe({
            player: "player1",
            eatingWay: "breadSelfTouching",
            targetPlayer: "player3",
            points: 9,
            gameID: 1,
            userID_0: 1,
            userID_1: 2,
            userID_2: 3,
            userID_3: 4,
            money: 32
        })
    })

})