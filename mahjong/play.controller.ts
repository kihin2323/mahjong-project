import express from "express"
import path from "path";
import PlayService from "./play.service";
import fs from "fs"
class PlayController {
    num
    picNameArr
    constructor(public playService: PlayService) {
        this.num = 0
        this.picNameArr = []
    }
    get = async (req: express.Request, res: express.Response) => {
        res.sendFile(path.resolve(__dirname, "./public/play.html"));
    }
    getcheckRecord = async (req: express.Request, res: express.Response) => {
        res.json(await this.playService.checkRecord(req.query.id as any));
    }

    postEatingWay = async (req: express.Request, res: express.Response) => {
        try {
            if (!req.body.eatingWay || req.body.points == "Choose..." || !req.body.points) {
                res
                    .status(400)
                    .json({ message: "請設定遊戲食法和番數" });
                return;
            }
            if (req.body.eatingWay == "breadSelfTouching" && !req.body.targetPlayer) {
                res
                    .status(400)
                    .json({ message: "請設定出銃者" });
                return;
            }
            if (req.body.eatingWay == "winningOnePlayer'" && !req.body.targetPlayer) {
                res
                    .status(400)
                    .json({ message: "請設定出銃者" });
                return;
            }
            let result = await this.playService.updateScore(req.body)
            return res.json(result);
        } catch (error) {
            console.log(error)
            return res.status(500).json(error.toString());
        }
    }

    // getUploadPhoto = async (req: express.Request, res: express.Response) => {
    //     try {
    //         res.json("picture")
    //     }
    //     catch (err) {
    //         res.status(500).json(err.toString())
    //     }
    // }

    postUploadPhoto = async (req: express.Request, res: express.Response) => {
        try {
            let filename = req.file?.filename
            if (!filename || typeof filename != "string" || filename.length == 0) {
                return res.status(400).json({message: "未有上載圖片/圖片格式不正確"})
            }
            this.picNameArr.push(filename)
            this.num += 1
            if(this.num === 2){
                this.num = 0
                let filenamePath = path.join('uploads', this.picNameArr[0]);
                this.picNameArr.shift()
                fs.unlinkSync(filenamePath);
                this.num += 1
            }
            return res.status(200).json({
                    message: "upload image succeeded",
                    filename: req.file.filename
                })
            
        } catch (error) {
            console.log(error)
            return res.status(500).json(error.toString());
        }
    }
}

export default PlayController;
