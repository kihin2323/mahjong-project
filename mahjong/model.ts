export interface Player{
    name: string 
    score: number
}

export interface MahjongPicture{
    name: string;
}