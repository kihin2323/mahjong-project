import express from 'express';
import gameController  from './game.controller';


export function gameRoute(
    gameController: gameController,
) {
    let gameRoutes = express.Router();
    gameRoutes.get('/game', gameController.get);
    gameRoutes.post('/delete-game-record/:id', gameController.deleteGameWhenDisconnect);
    gameRoutes.post('/start-game', gameController.post);
    gameRoutes.post('/delete-game/:id', gameController.deleteGame);
    return gameRoutes;
}
