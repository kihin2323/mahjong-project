import tornado.ioloop
import tornado.web
import json
import glob 
import random
# from PIL import Image
import cv2
import numpy as np
# import matplotlib 
import os
import tensorflow_hub as hub
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# from tensorflow.keras.models import load_model
from keras.preprocessing import image

# matplotlib.use('agg')
# model = load_model("./tiles")
tiles_model = hub.load("./tiles")

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

class DetectHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        print("setting headers!!!")
        self.set_header("access-control-allow-origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS')
        # HEADERS!
        self.set_header("Access-Control-Allow-Headers", "access-control-allow-origin,authorization,content-type")
    def options(self):
        # no body
        self.set_status(204)
        self.finish()

    def post(self):
        data = json.loads(self.request.body)
        print(data)
        # Load Yolo
        net = cv2.dnn.readNet("./yolov3_training_last.weights", "./yolov3_testing.cfg")
    
        # Name custom object
        classes = ["tile"]
        images_path = glob.glob(r"../uploads/" + data['filename'])
        layer_names = net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        # Insert here the path of your images
        random.shuffle(images_path)
        # loop through all the images
        for img_path in images_path:
            # Loading image
            img = cv2.imread(img_path)
            img = cv2.resize(img, None, fx=0.4, fy=0.4)
            height, width, channels = img.shape
        
            # Detecting objects
            blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
        
            net.setInput(blob)
            outs = net.forward(output_layers)
        
            # Showing informations on the screen
            classes = ['bamboo-1','bamboo-2','bamboo-3','bamboo-4','bamboo-5','bamboo-6',
                    'bamboo-7','bamboo-8','bamboo-9','char-1','char-2','char-3','char-4',
                    'char-5','char-6','char-7','char-8','char-9','dot-1','dot-2','dot-3',
                    'dot-4','dot-5','dot-6','dot-7','dot-8','dot-9','wind-east','dragon-green',
                    'dragon-red','wind-north','dragon-white','wind-south','wind-west']
            boxes = []
            eachClassify = []
            num = 0
            for out in outs:
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.3:
                        # Object detected
                        duplicated = False
                        center_x = int(detection[0] * width)
                        center_y = int(detection[1] * height)
                        w = int(detection[2] * width)
                        h = int(detection[3] * height)
        
                        # Rectangle coordinates
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)
                        for coord in boxes:
                            if((x - coord[0]) <= 70 and (x - coord[0]) >= 0):
                                duplicated = True
                            elif((coord[0] - x) <= 70 and (coord[0] - x) >= 0):
                                duplicated = True
                        if(duplicated == False and w < 160 and h < 200):
                            boxes.append([x, y, w, h])
            listBoxes = sorted(boxes,key=lambda l:l[0], reverse=False)
            for box in listBoxes:
                classifyingBox = []
                x,y,w,h = box
                crop_img = img[y:y+h, x:x+w]
                if(crop_img.any() and num < len(listBoxes)):
                    num = num + 1
                    cv2.imwrite("./cropped/{}.jpg".format(num), crop_img)
                    img_path = "./cropped/{}.jpg".format(num)
                    # image = Image.fromarray(crop_img)
                    # img = image.resize((224, 224), Image.BILINEAR) 
                    imgSet = image.load_img(img_path, target_size=(224, 224))
                    img_tensor = image.img_to_array(imgSet)  
                    # img_resized = cv2.resize(crop_img, (224, 224))
                    img_tensor = np.expand_dims(img_tensor, axis=0) 
                    img_tensor = img_tensor / 255.0
                    new_image = img_tensor
                    prediction = tiles_model(new_image)
                    for i in prediction[0]:
                        classifyingBox.append("{:.8f}".format(float(i)))
                    predicted = max(classifyingBox)
                    predictedIndex = classifyingBox.index(predicted)
                    eachClassify.append(classes[predictedIndex])
            print(eachClassify)
        
        cv2.destroyAllWindows()
        text = json.dumps(eachClassify)
        self.write(text)
        # self.write({"message:", eachClassify})
        # json_response=json.dumps(eachClassify)
        # response=Response(json_response,content_type='application/json; charset=utf-8')
        # response.headers.add('content-length',len(json_response))
        # response.status_code=200

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/detect", DetectHandler)
    ])

if __name__ == "__main__":
    app = make_app()
    print("listening on port 5000")
    app.listen(5000)
    tornado.ioloop.IOLoop.current().start()