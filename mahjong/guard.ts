import {Request, Response, NextFunction} from "express";

export function isUser (req:Request, res:Response, next: NextFunction){
    if(req.session && req.session.game || req.session && req.session.user){
        next();
    } else {
        res.redirect("/");
    }
}

