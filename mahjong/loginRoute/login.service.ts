import Knex from "knex";

class LoginService {
    constructor(public knex: Knex){}

    async toLogin(gameId:number){

    }
    checkEmail = async (email:string) => {
       let result =  await this.knex.select("password", "id")
        .from('user')
        .where('email',email)
        return result
    }
     getUser = async (email)=>{
        let result = await this.knex.select("password", "id")
        .from('user')
        .where('email',email)
        return result
    }
    toCreateUser = async (name, hashedPassword, picture, email)=>{
        let ids = await this.knex.insert({
            username: name,
            password: hashedPassword,
            profile_picture: picture,
            email
        }).into("user")
        .returning('id');
        return ids
    }
    fetchGameData = async (id)=>{
        let ids = await this.knex('game').select('id', 'created_at','fanso', 'money', 'updated_at').where('user_id', id);
        let user = await this.knex('user').select('username', 'profile_picture').where('id', id);
        return {ids, user}
    }
}
export default LoginService;