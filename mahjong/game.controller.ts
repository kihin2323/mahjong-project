import express from "express";
import GameService from "./game.service";
// import UploadPictureService from './uploadPicture.service';

class GameController {
  constructor(public gameService: GameService) {}

  get = async (req: express.Request, res: express.Response) => {
    try{
      let user_id = await this.gameService.checkGameOwner(req.query.id as any)
      if(user_id === null && req.session && req.session.game.id == req.query.id){
        res.json(await this.gameService.readGame(req.query.id as any));
      } else if(req.session && user_id === req.session.user.id){
        res.json(await this.gameService.readGame(req.query.id as any));
      } else {
        res.json({result: "not-found"})
      }

    
    return
    } catch (error) {
        return res.status(500).json(error.toString());
    }
  };
  
  post = async (req: express.Request, res: express.Response) => {
    try {
      if (!req.body.fanso || !req.body.money) {
        res
          .status(400)
          .json({ message: "請設定遊戲番數" });
        return;
      }
      if (
        !req.body.p1name ||
        !req.body.p2name ||
        !req.body.p3name ||
        !req.body.p4name       ) {
        res
          .status(400)
          .json({ message: "請輸入所有玩家暱稱" });
        return;
      }
      let ifLoggedIn
      if(req.session && req.session.user){
        ifLoggedIn = req.session.user.id
      } else {
        ifLoggedIn = null
      }
      let game_id = await this.gameService.startNewGame(req.body, ifLoggedIn);
      if (req.session){
        req.session.game = {
        id: game_id
        };
      }
      
      return res.json({game_id});
    
    } catch (error) {
      return res.status(500).json(error.toString());
    }
  };

  deleteGame = async (req: express.Request, res: express.Response)=>{
    if(req.session && req.session.user){
     let result = await this.gameService.deleteGameRecord(parseInt(req.params.id), parseInt(req.session.user.id))

     if(result){
       res.redirect("/")
     } else {
      res.redirect("/login")
     }
    }
  }

  deleteGameWhenDisconnect = async (req: express.Request, res: express.Response) => {
      await this.gameService.deleteGameRecordWhenDisconnect(parseInt(req.params.id))
    res.redirect("/")
  }
}


export default GameController;
