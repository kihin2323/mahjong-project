import Knex from "knex";
import knexconfigs from "./knexfile";
import UploadPictureService from './uploadPicture.service';

describe("upload picture test suite", ()=>{
    let knex: Knex;
    let uploadPictureService: UploadPictureService;

    beforeEach(async()=>{
        knex = Knex(knexconfigs.active);
        await knex("mahjong_picture").del();
        await knex("mahjong_picture").insert({
            name: "filename-trialpicture001.jpg"
        })
        uploadPictureService = new UploadPictureService(knex)
    })

    afterAll(()=>{
        knex.destroy();  
    })

    it("should be able to get the file name of the uploaded mahjong photo", async()=>{
        const uploadPhotoName = await uploadPictureService.getPicture()
        expect(uploadPhotoName).toHaveLength(1)
    })

    it("should be able to upload/post a new mahjong photo", async()=>{
        let originalPicture = await uploadPictureService.getPicture()
        const uploadPhotoNanme = await uploadPictureService.savePicture({
            mahjongPictureName: "filename-trialpicture002.jpg"
        })
        let newPicture = await knex("mahjong_picture")
            .where("name", uploadPhotoNanme)

        expect(newPicture).toHaveLength(1)
        let newLength = await uploadPictureService.getPicture()
        expect(newLength.length).toBe(originalPicture.length + 1)
        expect(newPicture[0].name).toBe("filename-trialpicture002.jpg")
    })
})