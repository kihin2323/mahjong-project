import express from 'express';
import { Multer } from "multer";
import UploadPictureController from './uploadPicture.controller';

export function uploadPictureRouter(
    uploadPictureController: UploadPictureController,
    upload: Multer,
) {
    let uploadPictureRoutes = express.Router();
    uploadPictureRoutes.get('/upload-photo', uploadPictureController.get);
    uploadPictureRoutes.post('/upload-photo', upload.single("filename"), uploadPictureController.post);

    return uploadPictureRoutes
}




