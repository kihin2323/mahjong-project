import express from 'express';
import bodyParser from 'body-parser';
import Knex from 'knex';
import expressSession from 'express-session';
import knexConfigs = require('./knexfile');
import multer from 'multer';
import grant from 'grant-express';
import dotenv from 'dotenv';
import fs from "fs";
let mode = process.env.NODE_ENV || "test";
let envFile = ".env." + mode; 
let envFileContent = fs.readFileSync(envFile).toString();
let env = dotenv.parse(envFileContent)

const app = express()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('public/login'));
app.use(express.static('public/register'));
app.use(express.static('pictures'));

app.use(expressSession({
  secret: "mahjong",
  resave: true,
  saveUninitialized: true
}));



app.use((req, res, next)=>{

  let something = grant({
    "defaults":{
        "protocol": req.headers.host === "localhost:8080"? "http": "https",
        "host": req.headers.host!,
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": env.GOOGLE_CLIENT_ID || "",
        "secret": env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
      },
  })
  something(req, res, next)
})


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/uploads`);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.jpg`);
  }
})
export const upload = multer({ storage: storage })
// upload.single('image')

let knexConfig = knexConfigs[mode]

export const knex = Knex(knexConfig)

app.use(express.static("uploads"))


// login route
import { loginRoute } from './loginRoute/login.routes';
import LoginService  from './loginRoute/login.service';
import LoginController from './loginRoute/login.controller';

let loginService = new LoginService(knex);
let loginController = new LoginController(loginService)
let loginRoutes = loginRoute(loginController);
app.use('/', loginRoutes)

// register route
import { registerRoute } from './registerRoute/register.routes';
import RegisterService  from './registerRoute/register.service';
import RegisterController from './registerRoute/register.controller';

let registerService = new RegisterService(knex);
let registerController = new RegisterController(registerService)
let registerRoutes = registerRoute(registerController);
app.use('/', registerRoutes)


import { gameRoute } from './game.routes';
import GameService  from './game.service';
import GameController from './game.controller';

let gameService = new GameService(knex);
let gameController = new GameController(gameService)
let gameRoutes = gameRoute(gameController);
app.use('/', gameRoutes)

// UploadPicture Routes
import { uploadPictureRouter } from './uploadPicture.routes';
import UploadPictureController from './uploadPicture.controller';
import UploadPictureService from './uploadPicture.service';

let uploadPictureService = new UploadPictureService(knex);
let uploadPictureController = new UploadPictureController(uploadPictureService);
let uploadPictureRoutes = uploadPictureRouter(uploadPictureController, upload);
app.use("/", uploadPictureRoutes)

// Play Routes
import PlayService from './play.service';
import PlayController from './play.controller';
import { playRoute } from './play.routes';

let playService = new PlayService(knex);
let playController = new PlayController(playService);
let playRoutes = playRoute(playController, upload)
app.use('/', playRoutes);


app.use((req, res) => {
  res.json('404 not found')
})

app.listen(8080, () => {
  console.log('listen on port http://localhost:8080')
})