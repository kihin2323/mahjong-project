import Knex from "knex";
export let gainMoney: number;
let playerScoreArr: any[] = [];

class PlayService {
  constructor(public knex: Knex) {}

  updateScore = async (body: {
    player;
    eatingWay;
    targetPlayer;
    points;
    gameID;
    userID_0;
    userID_1;
    userID_2;
    userID_3;
    money;
  }) => {
    const {
      player,
      eatingWay,
      targetPlayer,
      points,
      gameID,
      userID_0,
      userID_1,
      userID_2,
      userID_3,
      money,
    } = body;
    let moneyBase = money / 32;
    let playerScore: any[] = [];
    if (eatingWay == "selfTouching") {
      for (let i = 1; i <= 4; i++) {
        if (parseInt(player[player.length - 1]) !== i) {
          playerScore.push({
            [`player${i}`]: -calculationMoney(moneyBase, points) / 2,
          });
        } else {
          playerScore.push({
            [`player${i}`]: calculationMoney(moneyBase, points) * 1.5,
          });
        }
      }
      playerScoreArr.push(playerScore);
    }
    if (eatingWay == "breadSelfTouching") {
      for (let i = 1; i <= 4; i++) {
        if (parseInt(targetPlayer[targetPlayer.length - 1]) == i) {
          playerScore.push({
            [`player${i}`]: -(calculationMoney(moneyBase, points) * 1.5),
          });
        } else if (parseInt(player[player.length - 1]) == i) {
          playerScore.push({
            [`player${i}`]: calculationMoney(moneyBase, points) * 1.5,
          });
        } else {
          playerScore.push({
            [`player${i}`]: 0,
          });
        }
      }
      playerScoreArr.push(playerScore);
    }
    if (eatingWay == "winningOnePlayer") {
      for (let i = 1; i <= 4; i++) {
        if (parseInt(targetPlayer[targetPlayer.length - 1]) == i) {
          playerScore.push({
            [`player${i}`]: -calculationMoney(moneyBase, points),
          });
        } else if (parseInt(player[player.length - 1]) == i) {
          playerScore.push({
            [`player${i}`]: calculationMoney(moneyBase, points),
          });
        } else {
          playerScore.push({
            [`player${i}`]: 0,
          });
        }
      }
      playerScoreArr.push(playerScore);
    }
    if (eatingWay == "losingYourself") {
      for (let i = 1; i <= 4; i++) {
        if (parseInt(player[player.length - 1]) == i) {
          playerScore.push({
            [`player${i}`]: -calculationMoney(moneyBase, points) * 3,
          });
        } else {
          playerScore.push({
            [`player${i}`]: calculationMoney(moneyBase, points),
          });
        }
      }
      playerScoreArr.push(playerScore);
    }
    let round = await this.knex("round_score").max("round_num").where({game_id: gameID})
    let roundmax;
    if (!round[0].max) {
      roundmax = 1
    }else {
      roundmax = round[0].max + 1
    }
    await this.knex("round_score").insert({
      player_id: userID_0,
      score: playerScore[0].player1,
      game_id: gameID,
      round_num: roundmax,
    });
    await this.knex("round_score").insert({
      player_id: userID_1,
      score: playerScore[1].player2,
      game_id: gameID,
      round_num: roundmax,

    });
    await this.knex("round_score").insert({
      player_id: userID_2,
      score: playerScore[2].player3,
      game_id: gameID,
      round_num: roundmax,

    });
    await this.knex("round_score").insert({
      player_id: userID_3,
      score: playerScore[3].player4,
      game_id: gameID,
      round_num: roundmax,
    });

    let player1score = await this.knex("round_score")
      .sum("score")
      .where("game_id", gameID)
      .where("player_id", userID_0);
    await this.knex("player")
      .update({ score: player1score[0].sum })
      .where("id", userID_0);
    let player2score = await this.knex("round_score")
      .sum("score")
      .where("game_id", gameID)
      .where("player_id", userID_1);
    await this.knex("player")
      .update({ score: player2score[0].sum })
      .where("id", userID_1);
    let player3score = await this.knex("round_score")
      .sum("score")
      .where("game_id", gameID)
      .where("player_id", userID_2);
    await this.knex("player")
      .update({ score: player3score[0].sum })
      .where("id", userID_2);
    let player4score = await this.knex("round_score")
      .sum("score")
      .where("game_id", gameID)
      .where("player_id", userID_3);   
    await this.knex("player")
      .update({ score: player4score[0].sum })
      .where("id", userID_3);
    return {
      player1score: player1score[0].sum,
      player2score: player2score[0].sum,
      player3score: player3score[0].sum,
      player4score: player4score[0].sum
    };
  };

  async checkRecord(gameID) {
    const gameRecord = await this.knex("round_score")
    .select('round_score.game_id', 'player.name', 'round_score.score', 'round_score.round_num')
    .innerJoin('player', 'player.id', 'round_score.player_id')
    .where('round_score.game_id', gameID)
    .orderBy('round_score.round_num');
    return gameRecord
  }
}

export function calculationMoney(moneyBase, winning) {
  if (winning == 3) {
    return (gainMoney = moneyBase);
  }
  if (winning == 4) {
    return (gainMoney = 2 * moneyBase);
  }
  if (winning == 5) {
    return (gainMoney = 3 * moneyBase);
  }
  if (winning == 6) {
    return (gainMoney = 4 * moneyBase);
  }
  if (winning == 7) {
    return (gainMoney = 6 * moneyBase);
  }
  if (winning == 8) {
    return (gainMoney = 8 * moneyBase);
  }
  if (winning == 9) {
    return (gainMoney = 12 * moneyBase);
  }
  if (winning == 10) {
    return (gainMoney = 16 * moneyBase);
  }
  if (winning == 11) {
    return (gainMoney = 24 * moneyBase);
  }
  if (winning == 12) {
    return (gainMoney = 32 * moneyBase);
  }
  if (winning == 13) {
    return (gainMoney = 48 * moneyBase);
  }
}

export default PlayService;
