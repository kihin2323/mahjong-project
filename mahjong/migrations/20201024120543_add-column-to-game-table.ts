import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('game');
    if(hasTable){
        return knex.schema.table('game',(table)=>{
            table.integer("user_id").unsigned();
            table.foreign('user_id').references('user.id');
        });  
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("user");
    if(hasTable){
        return knex.schema.alterTable('user',(table)=>{
            table.dropColumn("user_id")
        });
    }else{
        return Promise.resolve();
    }
}

