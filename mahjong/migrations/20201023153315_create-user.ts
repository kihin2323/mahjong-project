import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("user");
    if(!hasTable){
        await knex.schema.createTable("user", (table)=>{
            table.increments();
            table.string("username");
            table.string("email")
            table.string("password")
            table.timestamps(false,true);
        })
    }else{
        return Promise.resolve();
    }
    
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists("user");
}

