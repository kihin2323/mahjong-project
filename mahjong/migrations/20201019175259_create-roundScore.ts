import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("round_score")){
        return;
    }
    await knex.schema.createTable("round_score", (table)=>{
        table.increments();
        table.integer("player_id").unsigned();
        table.foreign('player_id').references('player.id');
        table.integer('score')
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("round_score")
}

