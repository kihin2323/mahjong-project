import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('mahjongPicture')){
        return
    }
    await knex.schema.createTable('mahjongPicture', (table)=>{
        table.increments();
        table.text("name");
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('mahjongPicture')
}

