import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("mahjongPicture")){
        throw new Error("missing table mahjongPicture")
    }
    await knex.schema.renameTable("mahjongPicture", "mahjong_picture")
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.renameTable("mahjong_picture", "mahjongPicture")
}

