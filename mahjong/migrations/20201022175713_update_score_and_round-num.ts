import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("round_score")){
        throw new Error("missing table round_score")
    }
    await knex.schema.alterTable("round_score", (table)=>{
        table.integer("round_num").alter();
        table.decimal("score").alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table("round_score", (table)=>{
        table.decimal("round_num").alter();
        table.integer("score").alter();
    })
}

