import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("player")){
        return;
    }
    await knex.schema.createTable("player", (table)=>{
        table.increments();
        table.string("name");
        table.integer("score");
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("player")
}

