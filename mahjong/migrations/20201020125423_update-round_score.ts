import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("round_score")){
        throw new Error("missing table player")
    }
    await knex.schema.table('round_score',(table)=>{
        table.integer("game_id").unsigned();
        table.foreign('game_id').references('game.id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('round_score', (table)=>{
        table.dropColumn("game_id")
    })
}
