import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("game")){
        throw new Error("missing table game")
    }
    await knex.schema.table('game',(table)=>{
        table.renameColumn('farnso', 'fanso')

    })

}



export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('game', (table)=>{
        table.renameColumn('fanso', 'farnso')
    })
}

