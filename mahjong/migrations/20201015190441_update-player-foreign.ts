import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("player")){
        throw new Error("missing table player")
    }
    await knex.schema.table('player',(table)=>{
        table.integer("game_id").unsigned();
        table.foreign('game_id').references('game.id');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('player', (table)=>{
        table.dropColumn("game_id")
    })
}

