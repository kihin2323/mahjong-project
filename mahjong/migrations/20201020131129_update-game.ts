import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("game")){
        throw new Error("missing table game")
    }
    await knex.schema.table('game',(table)=>{
        table.integer("farnso");
        table.integer('money');
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('game', (table)=>{
        table.dropColumn("farnso")
        table.dropColumn("money")

    })
}

