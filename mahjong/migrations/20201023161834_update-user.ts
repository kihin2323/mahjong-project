import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('user');
    if(hasTable){
        return knex.schema.table('user',(table)=>{
            table.string('profile_picture')
        });  
    }else{
        return Promise.resolve();
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.table('user',(table)=>{
        table.dropColumn('profile_picture')
    });  
    
}

