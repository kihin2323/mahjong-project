import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("player")){
        throw new Error("missing table player")
    }
    await knex.schema.alterTable("player", (table)=>{
        table.decimal("score").alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable("player", (table)=>{
        table.integer("score").alter();
    })
}

