import Knex from "knex";
// import { Player } from './model'

class GameService {
    constructor(public knex: Knex){}

    async readGame(gameId:number){
            const games = 
            await this.knex('player').select('game.id', 'game.created_at','game.fanso', 'game.money', 'game.updated_at', 
            'player.id','player.name','player.score','player.game_id')
            .innerJoin('game', 'game.id','player.game_id')
            .where('game_id', gameId)
            return games
    }

    async startNewGame(body: {
        fanso:number
        money:number                    
        p1name:string
        p2name:string
        p3name:string
        p4name:string
        }, ifLoggedIn:number){
        const [game_id] = await this.knex("game").insert({fanso: body.fanso, money:body.money, user_id: ifLoggedIn})
        .returning('id');
        await this.knex("player").insert({name: body.p1name, score:0, game_id});
        await this.knex("player").insert({name: body.p2name, score:0, game_id});
        await this.knex("player").insert({name: body.p3name, score:0, game_id});
        await this.knex("player").insert({name: body.p4name, score:0, game_id});
        return game_id
    }
     checkGameOwner = async (id)=>{
        const [user_id] = await this.knex("game").select("user_id").where("id", id)
        return user_id.user_id
    }
    deleteGameRecordWhenDisconnect = async (id)=>{
        const [user_id] = await this.knex("game").select("user_id").where("id", id)
        if(user_id.user_id == null){
            await this.knex('round_score').where("game_id", id).del();
            await this.knex('player').where("game_id", id).del();
            await this.knex('game').where("id", id).del();
        }
        return true
    }
    async deleteGameRecord(id:number, userId:number){
        const [user_id] = await this.knex("game").select("user_id").where("id", id)
        if(user_id.user_id == userId && id){
            await this.knex('round_score').where("game_id", id).del();
            await this.knex('player').where("game_id", id).del();
            await this.knex('game').where("id", id).del();
            return true
        }
        return false
        
    }
}
export default GameService;


