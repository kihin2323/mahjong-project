import Knex from "knex";

class registerService {
    constructor(public knex: Knex){}

    async toRegister(username:string, password:string, profilePic:string, email:string){
       let ids = await this.knex.insert({
            username,
            password,
            profile_picture: profilePic,
            email
        }).into("user")
        .returning('id');
        return ids
    }
    async toCheckEmailAndUsername(email:string, username:string){
        const users = await this.knex.select('username')
                    .from('user')
                    .where('email',email)
                    .orWhere('username',username)
        return users
    }
}
export default registerService;
