import express from "express";
import registerService from "./register.service";
import path from "path";
import { hashPassword } from "../hash";


class registerController {
    constructor(public registerService: registerService) {}
  
    get = async (req: express.Request, res: express.Response) => {
      res.sendFile(path.resolve(__dirname, "../public/register/register.html"));
    };
    
    post = async (req: express.Request, res: express.Response) => {
      try {
        const { email, username, password, confirmPassword } = req.body;
        if (password !== confirmPassword) {
          return res.redirect("/register.html?error=確認密碼不同");
        }
        
        let checkEmailAndUsername = await this.registerService.toCheckEmailAndUsername(email, username)
        if (checkEmailAndUsername.length >= 1) {
          return res.redirect("/register.html?error=電子郵件或用戶名稱已被使用");
        }
        if (this.validUser(email, password)) {
          const hashedPassword = await hashPassword(password);
          await this.registerService.toRegister(username, hashedPassword, req.file?.filename || "anonymous.png", email)
        } else {
          res.redirect(
            "/register.html?error=請輸入有效密碼"
          );
        }
        res.redirect("/login?success=註冊成功");
      } catch (error) {
        return res.status(500).json(error.toString());
      }
    };
    validUser = (email, password) => {
      const validEmail = typeof email === "string" && email.trim() !== "";
      const validPassword =
        typeof password === "string" &&
        password.trim() !== "" &&
        password.trim().length >= 6;
      return validEmail && validPassword;
    };
  
  }
  
  export default registerController;
  