import express from 'express';
import registerController  from './register.controller';
import { upload } from "../main"


export function registerRoute(
    registerController: registerController,
) {
    let registerRoutes = express.Router();
    registerRoutes.get('/register', registerController.get);
    registerRoutes.post('/register', upload.single("profile-pic"),registerController.post);
    return registerRoutes;
}
