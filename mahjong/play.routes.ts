import express from 'express';
import { Multer } from "multer";
import playController from './play.controller';
import { isUser } from './guard';


export function playRoute(
    playController: playController,
    upload: Multer
) {
    let playRoutes = express.Router();
    playRoutes.get('/play', isUser, playController.get);
    playRoutes.post('/play/calculationPoint', playController.postEatingWay);
    playRoutes.get('/play/checkRecord', playController.getcheckRecord);
    playRoutes.post('/play/upload-photo', upload.single("filename"),playController.postUploadPhoto)
    return playRoutes;
}
