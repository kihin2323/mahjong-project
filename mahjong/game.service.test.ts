import Knex from "knex";
import knexconfigs from "./knexfile";
import GameService from './game.service';

describe("Game test suite", () => {
    let knex: Knex;
    let gameService: GameService;

    beforeEach(async () => {
        knex = Knex(knexconfigs.active);
        await knex("round_score").del();
        await knex("player").del();
        await knex("game").del();
        let [game_id] = await knex("game").insert({
            fanso:　8,
            money: 32,
            user_id: 1
        }).returning("id")

        await knex("player").insert({
            name: "player1",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player2",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player3",
            score: 0,
            game_id
        });
        await knex("player").insert({
            name: "player4",
            score: 0,
            game_id
        });
        gameService = new GameService(knex)
    })

    afterAll(() => {
        knex.destroy();
    });

    it("should get the information of game and the names of players (with req.session.user.id)", async () =>{
        const game_id = await gameService.startNewGame({
            fanso: 8, 
            money: 32,
            p1name: "player1", 
            p2name: "player2", 
            p3name: "player3", 
            p4name: "player4"
        }, 1)
        const get_game = await gameService.readGame(game_id)
        expect(get_game).toHaveLength(4)
    })

    it("should insert all players' names, scores and the corresponding game id (with req.session.user.id)", async () => {
        let game_id = await gameService.startNewGame({
            fanso: 8, 
            money: 32,
            p1name: "player1", 
            p2name: "player2", 
            p3name: "player3", 
            p4name: "player4"
        }, 1)
        await gameService.readGame(game_id)
        let newgame_id = await gameService.startNewGame({
            fanso: 10, 
            money: 96,
            p1name: "newplayer1", 
            p2name: "newplayer2", 
            p3name: "newplayer3", 
            p4name: "newplayer4"
        }, 1)
        await gameService.readGame(newgame_id)
        expect(newgame_id).toBe(game_id + 1)
    })
})