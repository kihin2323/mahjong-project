import express from "express";
import UploadPictureController from './uploadPicture.controller';
import UploadPictureService from './uploadPicture.service';
import knex from 'knex';

describe("uploadPicture controller test suite", () => {
    let getPicture: any;
    let savePicture: any;
    let uploadPictureController: UploadPictureController;
    let uploadPictureService: UploadPictureService = { knex, getPicture, savePicture } as any;
    let req: express.Request;
    let res: express.Response;

    beforeEach(() => {
        uploadPictureService = new UploadPictureService({} as knex)
        jest
            .spyOn(uploadPictureService, "getPicture")
            .mockImplementation(async () => [{
                name: "filename-trialPicture001"
            }])
        jest
            .spyOn(uploadPictureService, "savePicture")
            .mockImplementation(async () => [{
                name: "filename-trialPicture002"
            }])
        
        uploadPictureController = new UploadPictureController(uploadPictureService)
        req = {} as express.Request;
        res = {
            json: jest.fn(),
            status: jest.fn(() => {
                return res;
            }),
        } as any
    })


    it("should get the uploaded mahjong picture filename", async () => {
        await uploadPictureController.get(req, res);
        // expect(uploadPictureService.getPicture).toBeCalled();
        expect(res.json).toBeCalledWith([{ name: "filename-trialPicture001" }]);
    })

    it("should post/save a new mahjong picture", async () => {
        req.file = {
            filename: "filename-trialPicture001"
        } as any
        await uploadPictureController.post(req, res);
        // expect(uploadPictureService.savePicture).toBeCalled();
        // expect(res.status).toBe(200);
        expect(res.json).toBeCalledWith({
            success: true,
            message: "upload image successfully",
            filename: "filename-trialPicture001"
        })
    })
})


