// 計番表
let fanso;
    function updateFanso(button) {
        fanso = +button.textContent;
        document.querySelector(`[name="fanso"]`).value = fanso;
        document.querySelectorAll('.fanso').forEach((e) => (e.style.outline = ''));
        button.style.outline = '2px solid green';
        updateTable();
    }
    let money;
    function updateMoney(button) {
        money = +button.textContent;
        document.querySelector(`[name="money"]`).value = money;
        document.querySelectorAll('.money').forEach((e) => (e.style.outline = ''));
        button.style.outline = '2px solid green';
        updateTable();
    }
    function resetAll() {
        document.querySelector(`[name="fanso"]`).value = null;
        document.querySelector(`[name="money"]`).value = null;
        fanso = null;
        money = null;
        document.querySelectorAll('.fanso').forEach((e) => (e.style.outline = ''));
        document.querySelectorAll('.money').forEach((e) => (e.style.outline = ''));
        resetTable();
        resetAll();

    }

    let fanToOut = {
        3: 1,
        4: 2,
        5: 3,
        6: 4,
        7: 6,
        8: 8,
        9: 12,
        10: 16,
        11: 24,
        12: 32,
        13: 48,
    };
    function updateTable() {
        if (!fanso || !money) return;
        let result = document.querySelector('#result');
        let html = '';
        for (let fan = 3; fan <= fanso; fan++) {
        let out = (fanToOut[fan] / 32) * money;
        let self = (out / 2) * 3;
        html += `<tr>
            <td>${fan}</td>
            <td>${out}</td>
            <td>${self}</td>
            </tr>`;
        }
        result.innerHTML = html;
    }
    function resetTable() {
        let result = document.querySelector('#result');
        let html = '';
        result.innerHTML = html
    }



// dice-rolling function
function rollDice() {
    const dice = [...document.querySelectorAll(".die-list")];
    dice.forEach(die => {
      toggleClasses(die);
      die.dataset.roll = getRandomNumber(1, 6);
    });
    setTimeout(function(){
        showRollResult()
    }, 2000)
  }

  function toggleClasses(die) {
    die.classList.toggle("odd-roll");
    die.classList.toggle("even-roll");
  }

  function getRandomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

document.getElementById("roll-button").addEventListener("click", rollDice); 

// change the innerHTML of the rollResult
let rollResult = document.querySelector("#roll-result")
function showRollResult(){
    let dice1Result = parseInt(document.querySelector("#die-1").dataset.roll)
    let dice2Result = parseInt(document.querySelector("#die-2").dataset.roll)
    let dice3Result = parseInt(document.querySelector("#die-3").dataset.roll)
    rollResult.innerHTML = dice1Result + dice2Result + dice3Result
}


// error message
let postButton = document.querySelector(".open-table-button");
let postForm = document.querySelector("#start-game-form");
postButton.addEventListener("click", async function submitPost(event) {
    let errorBox = document.querySelector("#error-message");  
    event.preventDefault();
    event.stopPropagation();
    let formData = new FormData(postForm);
    let fetchResult = await fetch("/start-game", { 
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        }, 
        body: JSON.stringify({
            fanso,money,
            p1name: formData.get("p1name"),
            p2name: formData.get("p2name"),
            p3name: formData.get("p3name"),
            p4name: formData.get("p4name")
        }
    )});
    let result = await fetchResult.json();
    if (fetchResult.status == 400) {
        errorBox.classList.add("alert", "alert-danger");
        errorBox.textContent = result.message;
        errorBox.style.display = "block";
    } else {
        location.href="/play?game=" +  result.game_id;
        // history.pushState("","Mahjong", "/play?game=" +  result.game_id);
        return;
    }
})




// navbar profile things 

let loginDiv = document.querySelector(".login");

async function fetchLoginData(){
   let fetchDataJSON = await fetch("/fetch-user")
   let fetchData = await fetchDataJSON.json();


   let loginDivHTML = ''
    if(fetchData.id){
        let imageAddress = "";
        if (fetchData.result.user[0].profile_picture.slice(0, 5) === "https") {
          imageAddress = fetchData.result.user[0].profile_picture;
        } else {
          imageAddress = "/" + fetchData.result.user[0].profile_picture;
        }
        loginDivHTML += `<div class="navbar-brand profile"><a class="navbar-brand" id="login-link" href="/logout">登出</a><div class="profile-image"><img src="${imageAddress}"></div><p class="profile-name" >${fetchData.result.user[0].username}</p><div>`


        if(fetchData.result.ids.length > 0){
            loginDivHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">以往記錄</a>
          <div class="dropdown-menu scroll-box" aria-labelledby="navbarDropdown">`
          for(let i = 0; i <fetchData.result.ids.length; i++){
            let time = fetchData.result.ids[i].updated_at.slice(0,10)
            let fanso = fetchData.result.ids[i].fanso || 0
            let money = fetchData.result.ids[i].money || 0
            loginDivHTML += `<div class="grid-flex"><a class="dropdown-item record-grid" href="/play?game=${fetchData.result.ids[i].id}"><div class="record"><p>${time}</p>
            <p class="fanso-num">| 番數: ${fanso}</p>
            <p>| 倍數: ${money}</p></div></a><form class="trash" action="/delete-game/${fetchData.result.ids[i].id}" method="POST"> <a class="navbar-brand" id="back-home" href="/" data-toggle="modal" data-target="#back-modal"><i onclick="document.querySelectorAll('.trash').forEach((e)=>{e.submit()})" class="fas fa-trash"></i></a></form></div>`
          }
        }
        loginDivHTML += `</div>`
        
    } else {
        loginDivHTML +=  `<a class="navbar-brand" id="login-link" href="/login"><i class="fas fa-user"></i> 登入</a>
        <a class="navbar-brand" id="login-link" href="/register">註冊</a>`
    }
    loginDiv.innerHTML = loginDivHTML
}
fetchLoginData()

