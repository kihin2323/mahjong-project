window.onload = function () {
  const searchParams = new URLSearchParams(window.location.search);
  const errMessage = searchParams.get("error");

  if (errMessage) {
    const alertBox = document.createElement("div");
    alertBox.classList.add("alert", "alert-danger");
    alertBox.textContent = errMessage;
    document.querySelector("#error-message").appendChild(alertBox);
  } else {
    document.querySelector("#error-message").innerHTML = "";
  }
};




//  dice-rolling function
function rollDice() {
  const dice = [...document.querySelectorAll(".die-list")];
  dice.forEach(die => {
    toggleClasses(die);
    die.dataset.roll = getRandomNumber(1, 6);
  });
  setTimeout(function(){
      showRollResult()
  }, 2000)
}

function toggleClasses(die) {
  die.classList.toggle("odd-roll");
  die.classList.toggle("even-roll");
}

function getRandomNumber(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

document.getElementById("roll-button").addEventListener("click", rollDice); 


// change the innerHTML of the rollResult
let rollResult = document.querySelector("#roll-result")
function showRollResult(){
    let dice1Result = parseInt(document.querySelector("#die-1").dataset.roll)
    let dice2Result = parseInt(document.querySelector("#die-2").dataset.roll)
    let dice3Result = parseInt(document.querySelector("#die-3").dataset.roll)
    rollResult.innerHTML = dice1Result + dice2Result + dice3Result
}