let eat1;
let eat2;
let eat3;
let eat4;
let selfTouching = document.getElementById("selfTouching");
let breadSelfTouching = document.getElementById("breadSelfTouching");
let winning_one_player = document.getElementById("winningOnePlayer");
let losingYourself = document.getElementById("losingYourself");
let boxPlayerName = document.getElementById("boxPlayerName");
let playerContainer = document.querySelector(".players-container");
let pointsOption = document.querySelector(".pointsOption");
let targetPlayer;
let moneyBase = 4;
let player;
let eatingWay;
let playerScore;
let gameId
function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTableBody(table, data) {
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

const fetchGameRecord = async () => {
  let currentScore = document.querySelector(".currentScore")
  currentScore.innerHTML = ""
  const searchParams = new URLSearchParams(location.search);
  let gameID = searchParams.get("game");
  const data = await fetch("/play/checkRecord?id=" + gameID);
  const gameDetail = await data.json();
  let table = document.querySelector(".currentScore");
  // let record = Object.keys(gameDetail[0]);
  let roundset = new Set();
  let playerset = new Set();
  for (let gameRecord of gameDetail) {
    roundset.add(gameRecord.round_num)
    playerset.add(gameRecord.name)
  }
  let rows = []
  let players = Array.from(playerset)
  for (let round of roundset) {
    let p1 = turnInteger(gameDetail.find(record => record.round_num == round && record.name == players[0]).score)
    let p2 = turnInteger(gameDetail.find(record => record.round_num == round && record.name == players[1]).score)
    let p3 = turnInteger(gameDetail.find(record => record.round_num == round && record.name == players[2]).score)
    let p4 = turnInteger(gameDetail.find(record => record.round_num == round && record.name == players[3]).score)
    rows.push({
      round,
      p1,
      p2,
      p3,
      p4
    })

  }
  generateTableHead(table, ["局數", ...playerset]);
  generateTableBody(table, rows);
}

fetchGameRecord();

const fetchGameDetail = async () => {
  const searchParams = new URLSearchParams(location.search);
  let gameID = searchParams.get("game");
  gameId = gameID
  const data = await fetch("/game?id=" + gameID);
  const gameDetail = await data.json();
  playerContainer.innerHTML = `
    </br>
    <div class="container-1">
      <a id="eat1" data-toggle="modal" data-target="#exampleModalCenter">
        <div class="input-players">
          <div class="player-name-container">
            <span class="span-player-name">${gameDetail[0].name}</span>
          </div>
          <div class="player-score-container">
            <span class="player-score" id="playerOneScore">分數：${turnInteger(gameDetail[0].score)}</span>
          </div>
          <div class="player1-direction-container">東</div>
        </div>
      </a>
    </div>
    <div class="two-and-three">
      <div class="container-2">
        <a id="eat4" data-toggle="modal" data-target="#exampleModalCenter">
          <div class="input-players">
            <div class="player-name-container">
              <span class="span-player-name">${gameDetail[3].name}</span>
            </div>
            <div class="player-score-container">
              <span class="player-score" id="playerFourScore">分數：${turnInteger(gameDetail[3].score)}</span>
            </div>
            <div class="player4-direction-container">北</div>
          </div>
        </a>
      </div>
      <div class="open-table-container"></div>
      <div class="container-3">
        <a id="eat2" data-toggle="modal" data-target="#exampleModalCenter">
          <div class="input-players">
            <div class="player-name-container">
              <span class="span-player-name">${gameDetail[1].name}</span>
            </div>
            <div class="player-score-container">
              <span class="player-score" id="playerTwoScore">分數：${turnInteger(gameDetail[1].score)}</span>
            </div>
            <div class="player2-direction-container">南</div>
          </div>
        </a>
      </div>
    </div>
    <div class="container-4">
      <a id="eat3" data-toggle="modal" data-target="#exampleModalCenter">
        <div class="input-players">
          <div class="player-name-container">
            <span class="span-player-name">${gameDetail[2].name}</span>
          </div>
          <div class="player-score-container">
            <span class="player-score" id="playerThreeScore">分數：${turnInteger(gameDetail[2].score)}</span>
          </div>
          <div class="player3-direction-container">西</div>
        </div>
      </a>
    </div>
    `
  loopPoint();
  eat1 = document.getElementById("eat1");
  eat2 = document.getElementById("eat2");
  eat3 = document.getElementById("eat3");
  eat4 = document.getElementById("eat4");
  prepareGameData(eat1, eat2, eat3, eat4);
  playerScore = document.querySelectorAll(".player-score");
  checkScore()

};
fetchGameDetail();


function checkScore() {
  for (let i = 0; i < playerScore.length; i++) {
    htmlDiv = playerScore[i].innerHTML
    if ((htmlDiv.includes("-"))) {
      playerScore[i].classList.add('negative')
      playerScore[i].classList.remove('positive');
    } else {
      playerScore[i].classList.add('positive')
      playerScore[i].classList.remove("negative");
    }
  }
}




function turnInteger(num) {
  num = num.toString();
  return num = Math.round(Math.round(num * Math.pow(10, (1 || 0) + 1)) / 10) / Math.pow(10, (1 || 0));
  // let splitedNum1 = num.split(".")
  // if (parseInt(splitedNum1[1]) === 0) {
  //   return parseInt(splitedNum1[0])
  // }
  // return parseInt(num)
}
let aiFanso
async function loopPoint (){
  const searchParams = new URLSearchParams(location.search);
  let gameID = searchParams.get("game");
  const data = await fetch("/game?id=" + gameID);
  const gameDetail = await data.json();
  aiFanso = gameDetail
  let pointOption = pointsOption.innerHTML;
  pointOption += `<option selected>Choose...</option>`;
  for (i = 3; i <= gameDetail[0].fanso; i++) {
    pointOption += `<option value=${i}>${i}</option>`;
  }
  pointsOption.innerHTML = pointOption;
};

let gameDetail;

let saveGameID = document.querySelector("#get-game-id");

const searchParams = new URLSearchParams(location.search);
let gameID = searchParams.get("game");
async function prepareGameData(eat1, eat2, eat3, eat4) {
  const data = await fetch("/game?id=" + gameID);
  gameDetail = await data.json();

  saveGameID.innerHTML = gameID;

  if (eat1) {
    eat1.addEventListener("click", () => {
      player = "Player1";
      boxPlayerName.innerHTML = `<input type="hidden" name="player" value="player1">
            <input type="hidden" name="gameID" value="${gameID}">
            <input type="hidden" name="fanso" value="${gameDetail[0].fanso}">
            <input type="hidden" name="money" value="${gameDetail[0].money}">
            <input type="hidden" name="userID_0" value="${gameDetail[0].id}">
            <input type="hidden" name="userID_1" value="${gameDetail[1].id}">
            <input type="hidden" name="userID_2" value="${gameDetail[2].id}">
            <input type="hidden" name="userID_3" value="${gameDetail[3].id}">`;
    });
  }
  if (eat2) {
    eat2.addEventListener("click", () => {
      player = "Player2";
      boxPlayerName.innerHTML = `<input type="hidden" name="player" value="player2">
            <input type="hidden" name="gameID" value="${gameID}">
            <input type="hidden" name="fanso" value="${gameDetail[0].fanso}">
            <input type="hidden" name="money" value="${gameDetail[0].money}">
            <input type="hidden" name="userID_0" value="${gameDetail[0].id}">
            <input type="hidden" name="userID_1" value="${gameDetail[1].id}">
            <input type="hidden" name="userID_2" value="${gameDetail[2].id}">
            <input type="hidden" name="userID_3" value="${gameDetail[3].id}">`;
    });
  }
  if (eat3) {
    eat3.addEventListener("click", () => {
      player = "Player3";
      boxPlayerName.innerHTML = `<input type="hidden" name="player" value="player3">
            <input type="hidden" name="gameID" value="${gameID}">
            <input type="hidden" name="fanso" value="${gameDetail[0].fanso}">
            <input type="hidden" name="money" value="${gameDetail[0].money}">
            <input type="hidden" name="userID_0" value="${gameDetail[0].id}">
            <input type="hidden" name="userID_1" value="${gameDetail[1].id}">
            <input type="hidden" name="userID_2" value="${gameDetail[2].id}">
            <input type="hidden" name="userID_3" value="${gameDetail[3].id}">`;
    });
  }
  if (eat4) {
    eat4.addEventListener("click", () => {
      player = "Player4";
      boxPlayerName.innerHTML = `<input type="hidden" name="player" value="player4">
            <input type="hidden" name="gameID" value="${gameID}">
            <input type="hidden" name="fanso" value="${gameDetail[0].fanso}">
            <input type="hidden" name="money" value="${gameDetail[0].money}">
            <input type="hidden" name="userID_0" value="${gameDetail[0].id}">
            <input type="hidden" name="userID_1" value="${gameDetail[1].id}">
            <input type="hidden" name="userID_2" value="${gameDetail[2].id}">
            <input type="hidden" name="userID_3" value="${gameDetail[3].id}">`;
    });
  }
  winning_one_player.addEventListener("click", () => {
    winningOnePlayer(player);
  });
  breadSelfTouching.addEventListener("click", () => {
    winningYourself(player);
  });
}

async function winningOnePlayer(player) {
  let hiddenPlayer = document.querySelector(".hiddenPlayer");
  let hiddenPlayerHTML = '<div class="boxTitle">出銃者</div>';
  const searchParams = new URLSearchParams(location.search);
  let gameID = searchParams.get("game");
  const data = await fetch("/game?id=" + gameID);
  const gameDetail = await data.json();
  for (let i = 1; i <= 4; i++) {
    if (parseInt(player[player.length - 1]) !== i) {
      hiddenPlayerHTML += `<input type="radio" class="btn-point" name="targetPlayer" value="player${i}" id="">${gameDetail[i - 1].name
        }</input><br/>`;
    }
  }
  hiddenPlayer.innerHTML = hiddenPlayerHTML;
}

async function winningYourself(player) {
  let hiddenPlayer = document.querySelector(".hiddenPlayer");
  let hiddenPlayerHTML = '<div class="boxTitle">出銃者</div>';
  const searchParams = new URLSearchParams(location.search);
  let gameID = searchParams.get("game");
  const data = await fetch("/game?id=" + gameID);
  const gameDetail = await data.json();
  for (let i = 1; i <= 4; i++) {
    if (parseInt(player[player.length - 1]) !== i) {
      hiddenPlayerHTML += `<input type="radio" class="btn-point" name="targetPlayer" value="player${i}" id="">${gameDetail[i - 1].name
        }</input><br/>`;
    }
  }
  hiddenPlayer.innerHTML = hiddenPlayerHTML;
}

selfTouching.addEventListener("click", () => {
  let hiddenPlayer = document.querySelector(".hiddenPlayer");
  hiddenPlayer.innerHTML = "";
  eatingWay = "selfTouching";
});

losingYourself.addEventListener("click", () => {
  let hiddenPlayer = document.querySelector(".hiddenPlayer");
  hiddenPlayer.innerHTML = "";
  eatingWay = "losingYourself";
});

function calculationMoney(moneyBase, winning) {
  if (winning == 3) {
    return (gainMoney = moneyBase);
  }
  if (winning == 4) {
    return (gainMoney = 2 * moneyBase);
  }
  if (winning == 5) {
    return (gainMoney = 3 * moneyBase);
  }
  if (winning == 6) {
    return (gainMoney = 4 * moneyBase);
  }
  if (winning == 7) {
    return (gainMoney = 6 * moneyBase);
  }
  if (winning == 8) {
    return (gainMoney = 8 * moneyBase);
  }
  if (winning == 9) {
    return (gainMoney = 12 * moneyBase);
  }
  if (winning == 10) {
    return (gainMoney = 16 * moneyBase);
  }
  if (winning == 11) {
    return (gainMoney = 24 * moneyBase);
  }
  if (winning == 12) {
    return (gainMoney = 32 * moneyBase);
  }
  if (winning == 13) {
    return (gainMoney = 48 * moneyBase);
  }
}

// dice-rolling function
function rollDice() {
  const dice = [...document.querySelectorAll(".die-list")];
  dice.forEach(die => {
    toggleClasses(die);
    die.dataset.roll = getRandomNumber(1, 6);
  });
  setTimeout(function () {
    showRollResult()
  }, 2000)
}

function toggleClasses(die) {
  die.classList.toggle("odd-roll");
  die.classList.toggle("even-roll");
}

function getRandomNumber(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

document.getElementById("roll-button").addEventListener("click", rollDice);

// change the innerHTML of the rollResult
let rollResult = document.querySelector("#roll-result")
function showRollResult() {
  let dice1Result = parseInt(document.querySelector("#die-1").dataset.roll)
  let dice2Result = parseInt(document.querySelector("#die-2").dataset.roll)
  let dice3Result = parseInt(document.querySelector("#die-3").dataset.roll)
  rollResult.innerHTML = dice1Result + dice2Result + dice3Result
}

// appear upload photo option on value = "ai"
let aiPhotoContainer = document.querySelector(".ai-photo-container")
let introduction = document.querySelector(".ai-introduction");
let useAI = document.querySelector("#ai-use-button");
let tutorial = document.querySelector("#ai-tut-button");
let loginBtn = document.querySelector(".loginBtn")
useAI.addEventListener("click", () => {
  introduction.style.display = "none";
  useAI.style.display = "none";
  tutorial.style.display = "none";
  aiPhotoContainer.style.visibility = "visible";
})

let photoForm = document.querySelector("#upload-photo-form");
let submitAI = document.querySelector("#submit-photo-form");
let spinner = document.querySelector(".spinner-border");
let aiResult = document.querySelector(".ai-result");
let aiResultType = document.querySelector(".ai-result-type")
let aiText = document.querySelector("#analysis-text");
let aiModal = document.querySelector("#ai-modal");
let amendmentText = document.querySelector("#amendment-text");
let rightButton = document.querySelector("#right-result");
let label = document.querySelector("#my-file-label");

submitAI.addEventListener("click", async function submitPhoto(event) {
  let photoError = document.querySelector("#photo-error");
  event.preventDefault();
  event.stopPropagation();

  let photoFormData = new FormData(photoForm);

  let uploadResult = await fetch("/play/upload-photo", {
    method: "POST",
    body: photoFormData
  })
  let uploadResultJson = await uploadResult.json();

  if (uploadResult.status == 400) {
    photoError.classList.add("alert", "alert-danger");
    photoError.textContent = uploadResultJson.message;
    photoError.style.display = "block";
  } else {
    spinner.style.display = "block";
    aiText.style.display = "block";
    photoError.style.display = "none";
    label.style.display = "none";
    submitAI.style.display = "none";
  }
  let url = location.host === "localhost:8080" ? "http://localhost:5000/detect" : "https://mahjong-python.tk/detect"
  let resultJSON = await fetch(url, {
      headers: {
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify(uploadResultJson)
    })
    let result = await resultJSON.json();
    let obTile = breakArr(result)
        tileArr = obTile
        renderTile(tileArr)
    spinner.style.display = "none";
    aiText.style.display = "none";
    callbackDiv.style.display = "flex";
    aiResult.style.display = "block";
    aiResultType.style.display = "block";
    amendmentText.style.display = "block";
    rightButton.style.display = "block";
})

rightButton.addEventListener(("click"), ()=>{
  callbackDiv.style.display = "none";
  aiResultType.style.display = "none";
  aiResult.style.display = "none";
    amendmentText.style.display = "none";
    rightButton.style.display = "none";
    label.style.display = "initial";
    label.classList.add("adjustment")
    submitAI.style.display = "initial";
    outputImage.src = "";
})
// reset ai-modal and preview image
let outputImage = document.querySelector("#output-image")
$('#ai-modal').on('hidden.bs.modal', function () {
  document.querySelector("#myFile").value = "";
  outputImage.src = "";
});

// error message
let postButton = document.querySelector("#submit-form");
let postForm = document.querySelector("#start-winning");
let filename = document.querySelector("#myFile");

// close tutorial modal 
$("button[data-dismiss=modal2]").click(function () {
  $('.tutorial-modal').modal('hide');
})


// previewImage 
async function previewImage(event) {
  let photoReader = new FileReader();
  photoReader.onload = function () {
    outputImage.src = photoReader.result;
  }
  photoReader.readAsDataURL(event.target.files[0]);
}

//closeButton reset form 
let closeResetButton = document.querySelector("#close-reset")
closeResetButton.addEventListener("click", () => {
  document.getElementById('start-winning').reset();
  let hiddenPlayer = document.querySelector(".hiddenPlayer");
  hiddenPlayer.innerHTML = ``;
})

//submit score 
postButton.addEventListener("click", async function submitPost(event) {
  let errorBox = document.querySelector("#error-message");
  event.preventDefault();
  event.stopPropagation();
  let formData = new FormData(postForm);
  let fetchResult = await fetch("/play/calculationPoint", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      player,
      eatingWay: formData.get("eatingWay"),
      targetPlayer: formData.get("targetPlayer"),
      points: formData.get("points"),
      gameID: gameID,
      userID_0: gameDetail[0].id,
      userID_1: gameDetail[1].id,
      userID_2: gameDetail[2].id,
      userID_3: gameDetail[3].id,
      money: gameDetail[0].money,
    }),
  });
  let result = await fetchResult.json();
  if (fetchResult.status == 400) {
    errorBox.classList.add("alert", "alert-danger");
    errorBox.textContent = result.message;
    errorBox.style.display = "block";
  } else {
    let playerOneScore = document.querySelector("#playerOneScore");
    let playerTwoScore = document.querySelector("#playerTwoScore");
    let playerThreeScore = document.querySelector("#playerThreeScore");
    let playerFourScore = document.querySelector("#playerFourScore");
    let closeButton = document.querySelector("#exampleModalCenter");
    let hiddenPlayer = document.querySelector(".hiddenPlayer");
    playerOneScore.innerHTML = `<span>分數：${turnInteger(result.player1score)}</span>`;
    playerTwoScore.innerHTML = `<span>分數：${turnInteger(result.player2score)}</span>`;
    playerThreeScore.innerHTML = `<span>分數：${turnInteger(result.player3score)}</span>`;
    playerFourScore.innerHTML = `<span>分數：${turnInteger(result.player4score)}</span>`;
    hiddenPlayer.innerHTML = ``;
    let play1direction = document.querySelector(".player1-direction-container")
    let play2direction = document.querySelector(".player2-direction-container")
    let play3direction = document.querySelector(".player3-direction-container")
    let play4direction = document.querySelector(".player4-direction-container")
    function changeDirection() {
      if (play1direction.textContent == "東") {
        play1direction.innerHTML = '北'
        play2direction.innerHTML = "東"
        play3direction.innerHTML = "南"
        play4direction.innerHTML = "西"
      } else if (play2direction.textContent == "東") {
        play1direction.innerHTML = "西"
        play2direction.innerHTML = "北"
        play3direction.innerHTML = "東"
        play4direction.innerHTML = "南"
      }
      else if (play3direction.textContent == "東") {
        play1direction.innerHTML = "南"
        play2direction.innerHTML = "西"
        play3direction.innerHTML = "北"
        play4direction.innerHTML = "東"
      }
      else if (play4direction.textContent == "東") {
        play1direction.innerHTML = "東"
        play2direction.innerHTML = "南"
        play3direction.innerHTML = "西"
        play4direction.innerHTML = "北"
      }
    }
    errorBox.style.display = "none";
    function changeTableDirection() {
      let tableDirection = document.querySelector("#table-direction")
      if (tableDirection.textContent == "東風東圈") {
        tableDirection.innerHTML = "東風南圈"
      } else if (tableDirection.textContent == "東風南圈") {
        tableDirection.innerHTML = "東風西圈"
      } else if (tableDirection.textContent == "東風西圈") {
        tableDirection.innerHTML = "東風北圈"
      } else if (tableDirection.textContent == "東風北圈") {
        tableDirection.innerHTML = "南風東圈"
      } else if (tableDirection.textContent == "南風東圈") {
        tableDirection.innerHTML = "南風南圈"
      } else if (tableDirection.textContent == "南風南圈") {
        tableDirection.innerHTML = "南風西圈"
      } else if (tableDirection.textContent == "南風西圈") {
        tableDirection.innerHTML = "南風北圈"
      } else if (tableDirection.textContent == "南風北圈") {
        tableDirection.innerHTML = "北風東圈"
      } else if (tableDirection.textContent == "北風東圈") {
        tableDirection.innerHTML = "北風南圈"
      } else if (tableDirection.textContent == "北風南圈") {
        tableDirection.innerHTML = "北風西圈"
      } else if (tableDirection.textContent == "北風西圈") {
        tableDirection.innerHTML = "北風北圈"
      } else if (tableDirection.textContent == "北風北圈") {
        tableDirection.innerHTML = "東風東圈"
      }
    }
    if (player == "Player1" && play1direction.textContent == "東") {
    } else if (player == "Player2" && play2direction.textContent == "東") {
    } else if (player == "Player3" && play3direction.textContent == "東") {
    } else if (player == "Player4" && play4direction.textContent == "東") {
    } else {
      changeDirection();
      changeTableDirection()
    }
    document.getElementById('start-winning').reset();
    fetchGameRecord();
    checkScore();
    closeButton.click();
    // location.href = "/play?game=" + result.game_id;
    // history.pushState("","Mahjong", "/play?game=" +  result.game_id);
    return;
  }
}
);




// navbar profile things 

let loginDiv = document.querySelector(".login");

async function fetchLoginData() {
  let fetchDataJSON = await fetch("/fetch-user")
  let fetchData = await fetchDataJSON.json();


  let loginDivHTML = ''
  if (fetchData.id) {
    let imageAddress = "";
    if (fetchData.result.user[0].profile_picture.slice(0, 5) === "https") {
      imageAddress = fetchData.result.user[0].profile_picture;
    } else {
      imageAddress = "/" + fetchData.result.user[0].profile_picture;
    }
    loginDivHTML += `<div class="navbar-brand profile"><a class="navbar-brand" id="login-link" href="/logout">登出</a><div class="profile-image"><img src="${imageAddress}"></div><p class="profile-name" >${fetchData.result.user[0].username}</p><div>`


    if (fetchData.result.ids.length > 0) {
      loginBtn.style.display = "none"
      loginDivHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">以往記錄</a>
          <div class="dropdown-menu scroll-box" aria-labelledby="navbarDropdown">`
      for (let i = 0; i < fetchData.result.ids.length; i++) {
        let time = fetchData.result.ids[i].updated_at.slice(0, 10)
        let fanso = fetchData.result.ids[i].fanso || 0
        let money = fetchData.result.ids[i].money || 0
        loginDivHTML += `<div class="grid-flex"><a class="dropdown-item record-grid" href="/play?game=${fetchData.result.ids[i].id}"><div class="record"><p>${time}</p>
            <p class="fanso-num">| 番數: ${fanso}</p>
            <p>| 倍數: ${money}</p></div></a><form class="trash" action="/delete-game/${fetchData.result.ids[i].id}" method="POST"> <a class="navbar-brand" id="back-home" href="/" data-toggle="modal" data-target="#back-modal"><i onclick="document.querySelectorAll('.trash').forEach((e)=>{e.submit()})" class="fas fa-trash"></i></a></form></div>`
      }
    }
    loginDivHTML += `</div>`

  } else {
    loginDivHTML +=  `<a class="navbar-brand" id="login-link" href="/login"><i class="fas fa-user"></i> 登入</a>
    <a class="navbar-brand" id="login-link" href="/register">註冊</a>`
  }
  loginDiv.innerHTML = loginDivHTML
}
fetchLoginData()

// window.onbeforeunload = async function(){
//   await fetch("/delete-game-record",{ 
//     method: "POST"})
//   // deleteGameWhenDisconnect();
//   return 'Are you sure you want to leave?';
// };

let backToMain = document.querySelector("#back-to-main");

backToMain.addEventListener("click", () => {
  deleteGameWhenDisconnect();
})

async function deleteGameWhenDisconnect() {
  await fetch("/delete-game-record/" + gameId, {
    method: "POST"
  })
}

// redirect to main page if the user tries to enter the url of a non-existing game;
async function findNonExistingGame() {
  let currentLocation = window.location.href;
  let url = new URL(currentLocation)
  let params = url.searchParams.get("game")

  let gameID = searchParams.get("game");
  const data = await fetch("/game?id=" + gameID);
  if (data.status == 500) {
    window.location.href = "index.html";
  }
}

findNonExistingGame()