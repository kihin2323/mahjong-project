let outputImage = document.querySelector("#output-image")
let submitPhoto = document.querySelector("#submit-photo")

let uploadPhotoForm = document.querySelector("#upload-photo-form")
// let submitPhoto = document.querySelector("#submit-photo")

async function previewImage(event){
    let photoReader = new FileReader();
    photoReader.onload = function(){
    outputImage.src = photoReader.result;
    }
    photoReader.readAsDataURL(event.target.files[0]);
}


async function getPicture() {
    const response = await fetch("/upload-photo")
    const mahjongPicture = await response.json()
}

uploadPhotoForm.addEventListener("submit", async(event) => {
    event.preventDefault();
    const formData = new FormData(uploadPhotoForm)
    try{
        let uploadResult = await fetch("/upload-photo", {
            method: "POST",
            body: formData
        })
        let uploadResultJson = await uploadResult.json();
        
        let resultJSON = await fetch("http://localhost:5000/detect", {
            headers: {
                'Content-Type': 'application/json'
                },
            method: "POST",
            body: JSON.stringify(uploadResultJson)
        })
        
        let result = await resultJSON.json()
        let obTile = breakArr(result)
        tileArr = obTile
        renderTile(tileArr)
    }catch(err){
    }
    
})

let callAI = document.querySelector("#call-ai-button");
let overlayInterface = document.querySelector("#overlay-interface");




































































































// limit 





// the begin of hard code
// 十三么

// select the span for displaying the ai fanso
let aiFansoDisplay = document.querySelector("#ai-result-number");
// get max game fanso
// const searchParams = new URLSearchParams(location.search);
// let gameID = searchParams.get("game");
// gameId = gameID
// const data = await fetch("/game?id=" + gameID);
// const gameDetail = await data.json();
// aiFansoDisplay = gameDetail[0].fanso;

function thirteenOrphan(arr){
        let necessaryTile = [{type: "char", value: 1}, {type: "char", value: 9}, {type: "bamboo", value: 1},{type: "bamboo", value: 9}, {type: "dot", value: 1},{type: "dot", value: 9},{type: "direction", value: "east"},{type: "direction", value: "south"},{type: "direction", value: "west"},{type: "direction", value: "north"},{type: "dragon", value: "red"},{type: "dragon", value: "white"},{type: "dragon", value: "green"}]
        let copyArr = [...necessaryTile]
        let checkTile = []
        let result2
        for(let i = 0; i < arr.length; i++){
            result2 = necessaryTile.some((tile)=>{
                return tile.type === arr[i].type && tile.value === arr[i].value
            })
            if(necessaryTile.length > i){
                result1 = arr.some((tile)=>{return tile.type === necessaryTile[i].type && tile.value === necessaryTile[i].value })
            }
            
            checkTile.push(result1)
            checkTile.push(result2)
        }
        let allGood = checkTile.every((result)=>{
            return result === true
        })
        if(allGood){
            return true
        }else {
            return false
        }
        
}
let set101 = [{type: "char", value: 1}, {type: "char", value: 9}, {type: "bamboo", value: 1},{type: "bamboo", value: 9}, {type: "dot", value: 1},{type: "dot", value: 9},{type: "direction", value: "east"},{type: "direction", value: "south"},{type: "direction", value: "west"},{type: "direction", value: "north"},{type: "dragon", value: "red"},{type: "dragon", value: "white"},{type: "dragon", value: "green"},{type: "dragon", value: "green"}]

// 全么九
function checkOneAndNine(arr,resultSet1){
    let matchOneAndNine = true
    let checkThreeSet
    for(let k = 0 ; k < 4; k++){
        checkThreeSet = resultSet1[k] === "same"
        if(!checkThreeSet || resultSet1[resultSet1.length - 1] !== "isPair"){
            matchOneAndNine = false
        }
    }
    for(let i = 0; i < arr.length; i += 3){
        if(arr[i].value !== 1 && arr[i].value !== 9){
            matchOneAndNine = false
        }
    }
    if(matchOneAndNine){
        return true
    } else {
        return false
    }
}

let set100 = [{type: "bamboo", value: 1}, {type: "bamboo", value: 1}, {type: "bamboo", value: 1},{type: "bamboo", value: 9}, {type: "bamboo", value: 9},{type: "bamboo", value: 9},{type: "dot", value: 9},{type: "dot", value: 9},{type: "dot", value: 9},{type: "char", value: 1},{type: "char", value: 1},{type: "char", value: 1},{type: "char", value: 9},{type: "char", value: 9}]


// 大四喜
function bigFourJoys(arr, result2){
    let result3
    let correct = true
    let wind = ["east", "south", "west", "north"]
    for(let i = 0; i < 4; i++){
        result3 = arr.some((tile)=>{return tile.value === wind[i] })
        if(!result3){
            correct = false
        }
        if(result2[i] !== "same"){
            correct = false
        }
        if(result2[result2.length - 1] !== "isPair"){
            correct = false
        }
        
    }
    return correct
}

let set99 = [{type: "wind", value: "east"}, {type: "wind", value: "east"}, {type: "wind", value: "east"},{type: "wind", value: "south"}, {type: "wind", value: "south"},{type: "wind", value: "south"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "wind", value: "north"},{type: "wind", value: "north"},{type: "wind", value: "north"},{type: "char", value: 9},{type: "char", value: 9}]

// 小四喜
function tinyFourJoys(arr, result2){
    let result3
    let correct = true
    let checkWind = []
    let wind = ["east", "south", "west", "north"]
    for(let i = 0; i < 4; i++){
        result3 = arr.some((tile)=>{return tile.value === wind[i] })
        checkWind.push(result3)
        
    }
    if(result2[result2.length - 1] !== "isPair"){
        correct = false
    }
    result3 = []
    for(let k = 0; k < 4; k++){
        if(checkWind[k]){
            result3.push(result2[k] === "same")
        }else{
            let result5 = result2[k] === "same" || result2[k] === "serial"
            if(!result5){
                correct = false
            }
        }
    }
    let result4 = result3.every((wind)=>{
        return wind === true
    })
    let result6 = result3.filter((wind)=>{
        return wind === true
    })
    if(!result4 || result6.length !== 3){
        correct = false
    }
    return correct
    
}

let set98 = [{type: "wind", value: "east"}, {type: "wind", value: "east"}, {type: "wind", value: "east"},{type: "wind", value: "south"}, {type: "wind", value: "south"},{type: "wind", value: "south"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "bamboo", value: 1},{type: "bamboo", value: 2},{type: "bamboo", value: 3},{type: "char", value: 9},{type: "char", value: 9}]
// 大三元

function bigThreeYuen(arr, result2){
    let result3
    let correct = true
    let checkDragon = []
    let dragon = ["red", "white", "green", ""]
    for(let i = 0; i < dragon.length; i++){
        result3 = arr.some((tile)=>{return tile.value === dragon[i] })
        checkDragon.push(result3)
        
    }
    if(result2[result2.length - 1] !== "isPair"){
        correct = false
    }
    result3 = []
    for(let k = 0; k < 4; k++){
        if(checkDragon[k]){
            result3.push(result2[k] === "same")
        }else{
            let result5 = result2[k] === "same" || result2[k] === "serial"
            if(!result5){
                correct = false
            }
        }
    }
    let result4 = result3.every((dragon)=>{
        return dragon === true
    })
    let result6 = result3.filter((dragon)=>{
        return dragon === true
    })
    if(!result4 || result6.length !== 3){
        correct = false
    }
    return correct
    
}
let set97 = [{type: "dragon", value: "red"}, {type: "dragon", value: "red"}, {type: "dragon", value: "red"},{type: "dragon", value: "white"}, {type: "dragon", value: "white"},{type: "dragon", value: "white"},{type: "dragon", value: "green"},{type: "dragon", value: "green"},{type: "dragon", value: "green"},{type: "dot", value: 2},{type: "dot", value: 3},{type: "dot", value: 4},{type: "dot", value: 9},{type: "dot", value: 9}]
// 字一色
function allOneWord(result1, result2){
    let result3 = result2.filter((tileSet)=>{
        return tileSet === "same"
    })
    let correct = true
    
    if(result2[result2.length - 1] !== "isPair"){
        correct = false
    }
    let result5 = result1.every((tile)=>{
        return typeof(tile.value) !== "number"
    })
    if(result3.length !== 4 || !result5){
        correct = false
    }
    return correct
}
let set96 = [{type: "wind", value: "east"}, {type: "wind", value: "east"}, {type: "wind", value: "east"},{type: "wind", value: "south"}, {type: "wind", value: "south"},{type: "wind", value: "south"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "wind", value: "west"},{type: "dragon", value: "white"},{type: "dragon", value: "white"},{type: "dragon", value: "white"},{type: "dragon", value: "green"},{type: "dragon", value: "green"}]

// 清一色 10番
function checkIfSameType10(arr){
        let theType = arr[0].type
        let sameType = arr.every((tile)=>{
        return tile.type === theType
    })
        if(sameType){
            let set1 = threeTileSet(arr.slice(0,3))
            let set2 = threeTileSet(arr.slice(3,6))
            let set3 = threeTileSet(arr.slice(6,9))
            let set4 = threeTileSet(arr.slice(9,12))
            let set5 = twoTileSet(arr.slice(12,14))
            let correct = true
            let result = [set1, set2, set3, set4]
            if(set5 === "wrong"){
                correct = false
            }
            result1 = result.some((tileSet)=>{
                return tileSet === "wrong"
            })
            result2 = result.some((tileSet)=>{
                return tileSet === "serial"
            })
            if(result1 || result2 || !correct){
                return false
            }
            return true
        }
    
}


let set25 = [
    {type: "char", value: 1}, {type: "char", value: 1}, {type: "char", value: 1},
    {type: "char", value: 2},  {type: "char", value: 2}, {type: "char", value: 2}, {type:  "char", value: 6},
    {type:  "char", value: 6}, {type:  "char", value: 6},{type:  "char", value: 7},{type:  "char", value: 7},
    {type:  "char", value: 7}, {type:  "char", value: 3},{type:  "char", value: 3}
]


// 清一色 7番
function checkIfSameType7(arr){
    let theType = arr[0].type
    let sameType = arr.every((tile)=>{
    return tile.type === theType
})
    if(sameType){
        let set1 = threeTileSet(arr.slice(0,3))
        let set2 = threeTileSet(arr.slice(3,6))
        let set3 = threeTileSet(arr.slice(6,9))
        let set4 = threeTileSet(arr.slice(9,12))
        let set5 = twoTileSet(arr.slice(12,14))
        let result = [set1, set2, set3, set4, set5]
        result1 = result.some((tileSet)=>{
            return tileSet === "wrong"
        })
        if(result1){
            return false
        }
        return true
    }

}

let set2 = [
    {type: "char", value: 1}, {type: "char", value: 1}, {type: "char", value: 1},
    {type: "char", value: 2},  {type: "char", value: 3}, {type: "char", value: 4}, {type:  "char", value: 5},
    {type:  "char", value: 6}, {type:  "char", value: 7},{type:  "char", value: 9},{type:  "char", value: 9},
    {type:  "char", value: 9}, {type:  "char", value: 3},{type:  "char", value: 3}
]
// 小三元
function tinyThreeYuen(arr, result2){
    let result3
    let correct = true
    let checkDragon = []
    let dragon = ["red", "white", "green", ""]
    for(let i = 0; i < dragon.length; i++){
        result3 = arr.some((tile)=>{return tile.value === dragon[i] })
        checkDragon.push(result3)
        
    }
    if(result2[result2.length - 1] !== "isPair" && result3[result3.length - 1] === true){
        correct = false
    }
    result3 = []
    for(let k = 0; k < 4; k++){
        if(checkDragon[k]){
            result3.push(result2[k] === "same")
        }else{
            let result5 = result2[k] === "same" || result2[k] === "serial"
            if(!result5){
                correct = false
            }
        }
    }
    let result4 = result3.every((dragon)=>{
        return dragon === true
    })
    let result6 = result3.filter((dragon)=>{
        return dragon === true
    })
    if(!result4 || result6.length !== 3){
        correct = false
    }
    return correct
    
}
let set90 = [{type: "dragon", value: "red"},{type: "dragon", value: "red"},{type: "dragon", value: "red"},{type: "bamboo", value: 2}, {type: "bamboo", value: 2}, {type: "bamboo", value: 2},{type: "dragon", value: "green"},{type: "dragon", value: "green"},{type: "dragon", value: "green"},{type: "dot", value: 2},{type: "dot", value: 3},{type: "dot", value: 4},{type: "dragon", value: "white"},{type: "dragon", value: "white"}]


// 對對胡
function matchMatchWu(result2){
    let result3 = [...result2]
    let result4 = result3.pop()
    let result5 = result3.every((tileSet)=>{
        return tileSet === "same"
    })
    if(result4 !== "isPair" || !result5){
        return false
    }
    return true
}
let set81 = [{type: "char", value: 1},{type: "char", value: 1},{type: "char", value: 1},{type: "bamboo", value: 2}, {type: "bamboo", value: 2}, {type: "bamboo", value: 2},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "char", value: 7},{type: "char", value: 7},{type: "char", value: 7},{type: "dot", value: 9},{type: "dot", value: 9}]


// For more than 14 tiles set
// 十八羅漢
function tile18Set(result2){
    let correct = true
    if(result2[result2.length - 1] !== "isPair" || result2.length !== 5){
        correct = false
    }
    return correct
}
// 三槓子 10番
function tile17Set10Fan(arr, result2){
    try{
    let correct = true
    let num = 0
    let result3 = arr.every((tile)=>{
        return tile.type === arr[0].type
    })
    for(let i = 0; i < result2.length; i ++){
        if(result2[i] === "same"){
            num ++
        }
    }
    if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 1 || result3){
        correct = false
    }
    return correct
    }catch(e){
        return false
    }

}

let set33 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 9},{type: "dot", value: 9}]
// 三槓子 7番
function tile17Set7Fan(arr, result2){
    try{
        let correct = true
        let result3 = arr.every((tile)=>{
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet)=>{
            return tileSet === "wrong"
        })
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
    
}

let set56 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 7},{type: "dot", value: 8},{type: "dot", value: 9},{type: "dot", value: 9},{type: "dot", value: 9}]
// 三槓子 3番
function tile17Set3Fan(result2){
    try{
        let correct = true
        let num = 0
        for(let i = 0; i < result2.length; i++){
            if(result2[i] === "same"){
                num ++
            }
        }
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 1){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
    
}

let set53 = [{type: "char", value: 2}, {type: "char", value: 2}, {type: "char", value: 2},{type: "char", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "char", value: 8},{type: "char", value: 8},{type: "char", value: 8},{type: "dot", value: 9},{type: "dot", value: 9}]
// 二槓子 10番
function tile16Set10Fan(arr, result2){
    try{
        let correct = true
        let num = 0
        let result3 = arr.every((tile)=>{
            return tile.type === arr[0].type
        })
        for(let i = 0; i < result2.length; i ++){
            if(result2[i] === "same"){
                num ++
            }
        }
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || num !== 2){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
}
let set28 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 9},{type: "dot", value: 9}]
// 二槓子 7番
function tile16Set7Fan(arr, result2){
    try{
        let correct = true
        let result3 = arr.every((tile)=>{
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet)=>{
            return tileSet === "wrong"
        })
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
}
let set37 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 7},{type: "dot", value: 8},{type: "dot", value: 9},{type: "dot", value: 9},{type: "dot", value: 9}]
// 二槓子 3番
function tile16Set3Fan(result2){
    try{
        let correct = true
        let num = 0
        for(let i = 0; i < result2.length; i++){
            if(result2[i] === "same"){
                num ++
            }
        }
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 2){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
    
}

let set57 = [{type: "char", value: 2}, {type: "char", value: 2}, {type: "char", value: 2},{type: "char", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "char", value: 8},{type: "char", value: 8},{type: "char", value: 8},{type: "dot", value: 9},{type: "dot", value: 9}]
// 一槓子 10番
function tile15Set10Fan(arr, result2){
    try{
        let correct = true
        let num = 0
        let result3 = arr.every((tile)=>{
            return tile.type === arr[0].type
        })
        for(let i = 0; i < result2.length; i ++){
            if(result2[i] === "same"){
                num ++
            }
        }
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || num !== 3){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
}
let set21 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 6},{type: "dot", value: 9},{type: "dot", value: 9}]
// 一槓子 7番
function tile15Set7Fan(arr, result2){
    try{
        let correct = true
        let result3 = arr.every((tile)=>{
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet)=>{
            return tileSet === "wrong"
        })
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
}
let set311 = [{type: "dot", value: 2}, {type: "dot", value: 2}, {type: "dot", value: 2},{type: "dot", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 7},{type: "dot", value: 8},{type: "dot", value: 9},{type: "dot", value: 9},{type: "dot", value: 9}]
// 一槓子 3番
function tile15Set3Fan(result2){
    try{
        let correct = true
        let num = 0
        for(let i = 0; i < result2.length; i++){
            if(result2[i] === "same"){
                num ++
            }
        }
        if(result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 3){
            correct = false
        }
        return correct
        }catch(e){
            return false
        }
    
}

let set537 = [{type: "char", value: 2}, {type: "char", value: 2}, {type: "char", value: 2},{type: "char", value: 2}, {type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 3},{type: "dot", value: 5},{type: "dot", value: 5},{type: "dot", value: 5},{type: "char", value: 8},{type: "char", value: 8},{type: "char", value: 8},{type: "dot", value: 9},{type: "dot", value: 9}]
// 混一色
function blendOneColor(arr,result2){
    let result3
    let result4
    let typeArr = []
    let anotherTypeArr = []
    let threeType = ["dot", "bamboo", "char"]
    let anthorType = ["wind", "dragon"]
    for(let i = 0; i < threeType.length; i++){
        result3 = arr.some((tile)=>{
            return tile.type === threeType[i]
        })
        if(i < anthorType.length){
            result4 = arr.some((tile)=>{
                return tile.type === anthorType[i]
            })
            anotherTypeArr.push(result4)
        }
        typeArr.push(result3)
    }
    let result6 = anotherTypeArr.some((result)=>{
        return result === true
    })
    let result5 = typeArr.filter((result)=>{
        return result === true
    })
    let result7 = result2.some((tileSet)=>{
        return tileSet === "wrong"
    })
    if(result5.length !== 1 || !result6 || result7){
        return false
    }
    return true
}

let set137 = [{type: "wind", value: "south"}, {type: "wind", value: "south"}, {type: "wind", value: "south"},{type: "bamboo", value: 1}, {type: "bamboo", value: 1},{type: "bamboo", value: 1},{type: "bamboo", value: 6},{type: "bamboo", value: 7},{type: "bamboo", value: 8},{type: "bamboo", value: 4},{type: "bamboo", value: 4},{type: "bamboo", value: 4},{type: "bamboo", value: 5},{type: "bamboo", value: 5}]
// 平胡
function plainWu(result2){
    try{
    for(let i = 0; i < 4; i++){
        if(result2[i] !== "serial"){
            correct = false
        }
    }
    let correct = true
    if(result2[result2.length - 1] !== "isPair"){
        correct = false
    }
    return correct
    }catch(e){
        return false
    }
}

let set111 = [{type: "dot", value: 1}, {type: "dot", value: 2}, {type: "dot", value: 3},{type: "bamboo", value: 1}, {type: "bamboo", value: 2},{type: "bamboo", value: 3},{type: "bamboo", value: 8},{type: "bamboo", value: 7},{type: "bamboo", value: 6},{type: "char", value: 4},{type: "char", value: 5},{type: "char", value: 6},{type: "bamboo", value: 5},{type: "bamboo", value: 5}]




// 花么
function flowerYu(arr, result2){
    let correct = true
    for(let i = 0; i < arr.length; i++){
        if(arr[i].value === i && i !== 1 && i !== 9){
            correct = false
        }
        arr[i]
    }
    let result3 = result2.some((tileSet)=>{
        return tileSet === "wrong"
    })
    let result4 = result2.some((tileSet)=>{
        return tileSet === "serial"
    })
    if(result3 || result4){
        correct = false
    }
    return correct
}

let set323 = [{type: "dot", value: 9}, {type: "dot", value: 9}, {type: "dot", value: 9},{type: "bamboo", value: 9}, {type: "bamboo", value: 9},{type: "bamboo", value: 9},{type: "bamboo", value: 1},{type: "bamboo", value: 1},{type: "bamboo", value: 1},{type: "wind", value: "east"},{type: "wind", value: "east"},{type: "wind", value: "east"},{type: "dragon", value: "red"},{type: "dragon", value: "red"}]

// 中發白

function middleFanWhite(arr){
    let result = categorizeToArr(arr)
    let result2 = checkSetFunc(result)
    let correct = false
    
    for(let i = 0; i < result2.length ; i++){
        if(result2[i] === "same" && result[i][0].type === "dragon"){
            correct = true
        }
    }
    let result3 = result2.some((tileSet)=>{
        return tileSet === "wrong"
    })
    if(result3){
        correct = false
    }

    return correct
}
let set1000 = [{type: "dot", value: 9}, {type: "dot", value: 9}, {type: "dot", value: 9},{type: "bamboo", value: 9}, {type: "bamboo", value: 9},{type: "bamboo", value: 9},{type: "bamboo", value: 1},{type: "bamboo", value: 1},{type: "bamboo", value: 1},{type: "dragon", value: "white"},{type: "dragon", value: "white"},{type: "dragon", value: "white"},{type: "dragon", value: "green"},{type: "dragon", value: "green"}]
// end of hard code
function filterArrLength(arr){
    try{
    let result = categorizeToArr(arr)
    let result2 = checkSetFunc(result)
    if(arr.length === 18){
        let tile18SetVar = tile18Set(result2)
    }else if(arr.length === 17){
        let tile17Set10FanVar = tile17Set10Fan(arr, result2)
        let tile17Set7FanVar = tile17Set7Fan(arr, result2)
        let tile17Set3FanVar = tile17Set3Fan(result2)
    } else if(arr.length === 16){
        let tile16Set10FanVar = tile16Set10Fan(arr, result2)
        let tile16Set7FanVar = tile16Set7Fan(arr, result2)
        let tile16Set3FanVar = tile16Set3Fan(result2)
    }else if(arr.length === 15){
        let tile15Set10FanVar = tile15Set10Fan(arr, result2)
        let tile15Set7FanVar = tile15Set7Fan(arr, result2)
        let tile15Set3FanVar = tile15Set3Fan(result2)
    }else{
        let thirteenOrphanVar = thirteenOrphan(arr)
        let checkOneAndNineVar = checkOneAndNine(arr, result2)
        let bigFourJoysVar = bigFourJoys(arr, result2)
        let tinyFourJoysVar = tinyFourJoys(arr , result2)
        let bigThreeYuenVar = bigThreeYuen(arr , result2)
        let allOneWordVar = allOneWord(result,result2)
        let checkIfSameType10Var = checkIfSameType10(arr)
        let checkIfSameType7Var = checkIfSameType7(arr)
        let tinyThreeYuenVar = tinyThreeYuen(arr, result2)
        let matchMatchWuVar = matchMatchWu(result2)
        let blendOneColorVar = blendOneColor(arr , result2)
        let plainWuVar = plainWu(result2)
        let flowerYuVar = flowerYu(arr, result2)
    }
    }catch(e){
        return false
    }
}

function categorizeToArr(resultArr){
    let setTile = []
    let filteredArr
    for(let i = 0; i < resultArr.length; i++){
    filteredArr = resultArr.filter((tile)=>{ return tile.type === resultArr[i].type && tile.value === resultArr[i].value})
        if(resultArr.length > 14){
            let checkIfPushedFour = setTile.some((tileArr)=>{
                return tileArr.includes(filteredArr[0])
            })
            if(filteredArr.length === 4 && !checkIfPushedFour){
                setTile.push(filteredArr)
            }
        }
    }
if(resultArr.length === 18){
    let eyeSet = resultArr.slice(resultArr.length - 2, resultArr.length)
    setTile.push(eyeSet)
    return setTile
}else if(resultArr.length > 14){
    let arrLength = resultArr.length
    let eyeSet = resultArr.slice(resultArr.length - 2, resultArr.length)
    let copyResultArr = [...resultArr]
    copyResultArr.pop()
    copyResultArr.pop()
    for(let i = 0 ; i < 4 ; i ++){
        copyResultArr = copyResultArr.filter((tileSet)=>{
            return tileSet != setTile[0][i]
        })
        if(arrLength > 15){
            copyResultArr = copyResultArr.filter((tileSet)=>{
                return tileSet != setTile[1][i]
            })
        }
        
        if(arrLength > 16){
            copyResultArr = copyResultArr.filter((tileSet)=>{
                return tileSet != setTile[2][i]
            })
        }
        
    }
    if(arrLength === 17){
        copyResultArr = copyResultArr.splice(0,3)
    }
    if(arrLength === 16){
        let threeSetArr1 = copyResultArr.splice(0,3)
        setTile.push(threeSetArr1)
    }
    if(arrLength === 15){
        let threeSetArr1 = copyResultArr.splice(0,3)
        let threeSetArr2 = copyResultArr.splice(0,3)
        setTile.push(threeSetArr1)
        setTile.push(threeSetArr2)
    }
    setTile.push(copyResultArr)
    setTile.push(eyeSet)
    return setTile
} 
    let set1 = resultArr.slice(0,3)
    let set2 = resultArr.slice(3,6)
    let set3 = resultArr.slice(6,9)
    let set4 = resultArr.slice(9,12)
    setTile.push(set1)
    setTile.push(set2)
    setTile.push(set3)
    setTile.push(set4)
    setTile.push(resultArr.slice(12,14))
    return setTile
}


function checkSetFunc(arr){
    let cateArr = []
    for(let i = 0; i < arr.length; i++){
        if(arr[i].length === 4){
            cateArr.push(fourTileSet(arr[i]))
        }else if(arr[i].length === 3){
            cateArr.push(threeTileSet(arr[i]))
        }else if(arr[i].length === 2){
            cateArr.push(twoTileSet(arr[i]))
        }
    }
    return cateArr
}


function fourTileSet(fourArr){
    let checkType = fourArr[0].type === fourArr[1].type && fourArr[1].type === fourArr[2].type && fourArr[2].type === fourArr[3].type
    let checkValue = fourArr[0].value === fourArr[1].value && fourArr[1].value === fourArr[2].value && fourArr[2].value === fourArr[3].value
    if(checkValue && checkType){
        return "FourIsSame"
    }else {
        return "wrong"
    }
}
function threeTileSet(threeArr){
    let checkType = threeArr[0].type === threeArr[1].type && threeArr[1].type === threeArr[2].type
    let setCondition
    if(checkType){
      let sameThreeTile = threeArr[0].value === threeArr[1].value && threeArr[1].value === threeArr[2].value
      let serialThreeTile = threeArr[1].value === threeArr[0].value + 1 && threeArr[2].value === threeArr[0].value + 2
      let serialThreeTileReverse = threeArr[1].value === threeArr[0].value - 1 && threeArr[2].value === threeArr[0].value - 2
      
      if(sameThreeTile){
        setCondition = "same"
      } else if (serialThreeTile || serialThreeTileReverse){
        setCondition = "serial"
      } else {
        setCondition = "wrong"
      }
    } else {
        setCondition = "wrong"
    }
    return setCondition
}

function twoTileSet(eyeArr){
    let checkIfPair = eyeArr[0].type === eyeArr[1].type && eyeArr[0].value === eyeArr[1].value
    if(checkIfPair){
        return "isPair"
    } else {
        return "wrong"
    } 
}







// let wholeSet = ["dot-1", "dot-2","dot-3","dot-4","dot-5","dot-6","dot-7","dot-8","dot-9", "bamboo-1","bamboo-2","bamboo-3","bamboo-4","bamboo-5","bamboo-6","bamboo-7","bamboo-8","bamboo-9", "char-1","char-2","char-3","char-4","char-5","char-6","char-7","char-8","char-9","dragon-red","dragon-green","dragon-white", "wind-east","wind-west","wind-south","wind-north"]
// let callbackDiv = document.querySelector("#callback")
// let hiddenDiv = document.querySelector(".hidden")
// let whiteBackGround = document.querySelector(".white-background")
// let obTiles = breakArr(wholeSet)
// let whiteBackGroundHTML = ''
// let wrongTile
// let tileArr
// function renderOptionTile(obTiles){
// for(let a = 0; a < obTiles.length ; a++){
//     whiteBackGroundHTML += `<div><img class="option-tile ${obTiles[a]["type"]}-${obTiles[a]["value"]}-className" src="${obTiles[a]["type"]}-${obTiles[a]["value"]}.png"></div>`
// }
// whiteBackGround.innerHTML = whiteBackGroundHTML
//     let OptionTileImages = document.querySelectorAll(".option-tile")
//     OptionTileImages.forEach((image)=>{
//         image.addEventListener("click", (e)=>{
//             let indexNum1 = wrongTile.split("-")
//             let indexNum = parseInt(indexNum1)
//             let nameImage = e.target.classList[1].split("-")
//             tileArr[indexNum] = {
//                 type: nameImage[0],
//                 value: parseInt(nameImage[1]) || nameImage[1]
//             }
//             hiddenDiv.classList.remove("show-div");
//             renderTile(tileArr)
            
//         })
//     })
// }
// renderOptionTile(obTiles)
// function breakArr(arr){
//     let obArr = []
//     let splitedStr
//     for(let i = 0; i < arr.length; i++){
//         splitedStr = arr[i].split("-")
//         obArr.push({
//             type: splitedStr[0],
//             value: parseInt(splitedStr[1]) || splitedStr[1]
//         })
//     }
//     return obArr
// }
// function renderTile(arr){
//     let callbackDivHTML = ''
//     for(let i = 0; i < arr.length; i++){
//         callbackDivHTML += `<div><img class="tile-image ${i}-index" src="${arr[i]["type"]}-${arr[i]["value"]}.png"></div>`
//     }
//     callbackDiv.innerHTML = callbackDivHTML
//     let tileImages = document.querySelectorAll(".tile-image")
//     tileImages.forEach((image)=>{
//         image.addEventListener("click", (e)=>{
//             hiddenDiv.classList.add("show-div");
//             wrongTile = e.target.classList[1]
//         })
//     })
    
//     hiddenDiv.addEventListener("click", (e) => {
//         if (e.target.classList[0] === "grey-area" || e.target.classList[1] === "show-div") {
//             hiddenDiv.classList.remove("show-div");
//         }
//       });
    
// }

