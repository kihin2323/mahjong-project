let wholeSet = ["dot-1", "dot-2", "dot-3", "dot-4", "dot-5", "dot-6", "dot-7", "dot-8", "dot-9", "bamboo-1", "bamboo-2", "bamboo-3", "bamboo-4", "bamboo-5", "bamboo-6", "bamboo-7", "bamboo-8", "bamboo-9", "char-1", "char-2", "char-3", "char-4", "char-5", "char-6", "char-7", "char-8", "char-9", "dragon-red", "dragon-green", "dragon-white", "wind-east", "wind-west", "wind-south", "wind-north"]

let callbackDiv = document.querySelector("#callback")
let hiddenDiv = document.querySelector(".hidden")
let whiteBackGround = document.querySelector(".white-background")
let obTiles = breakArr(wholeSet)
let whiteBackGroundHTML = ''
let wrongTile
let tileArr

function renderOptionTile(obTiles) {
    for (let a = 0; a < obTiles.length; a++) {
        whiteBackGroundHTML += `<div><img class="option-tile ${obTiles[a]["type"]}-${obTiles[a]["value"]}-className" src="${obTiles[a]["type"]}-${obTiles[a]["value"]}.png"></div>`
    }
    whiteBackGround.innerHTML = whiteBackGroundHTML
    let OptionTileImages = document.querySelectorAll(".option-tile")
    OptionTileImages.forEach((image) => {
        image.addEventListener("click", (e) => {
            let indexNum1 = wrongTile.split("-")
            let indexNum = parseInt(indexNum1)
            let nameImage = e.target.classList[1].split("-")
            tileArr[indexNum] = {
                type: nameImage[0],
                value: parseInt(nameImage[1]) || nameImage[1]
            }
            hiddenDiv.classList.remove("show-div");
            renderTile(tileArr)
        })
    })
}
renderOptionTile(obTiles)

function breakArr(arr) {
    let obArr = []
    let splitedStr
    for (let i = 0; i < arr.length; i++) {
        splitedStr = arr[i].split("-")
        obArr.push({
            type: splitedStr[0],
            value: parseInt(splitedStr[1]) || splitedStr[1]
        })
    }
    return obArr
}

function renderTile(arr) {
    let callbackDivHTML = ''
    for (let i = 0; i < arr.length; i++) {
        callbackDivHTML += `<div><img class="tile-image ${i}-index" src="${arr[i]["type"]}-${arr[i]["value"]}.png"></div>`
    }
    callbackDiv.innerHTML = callbackDivHTML
    let tileImages = document.querySelectorAll(".tile-image")
    tileImages.forEach((image) => {
        image.addEventListener("click", (e) => {
            hiddenDiv.classList.add("show-div");
            wrongTile = e.target.classList[1]
        })
    })

    hiddenDiv.addEventListener("click", (e) => {
        if (e.target.classList[0] === "grey-area" || e.target.classList[1] === "show-div") {
            hiddenDiv.classList.remove("show-div");
        }
    });
    filterArrLength(arr)

}

// the final result from clicking the right button 
let rightResult = document.querySelector("#right-result");
rightResult.addEventListener("click", async (event) => {
    event.preventDefault();
    event.stopPropagation();

    let callBackChild = callbackDiv.childNodes
})




// the begin of hard code
// 十三么

// select the span for displaying the ai fanso
let aiFansoDisplay = document.querySelector("#ai-result-number");
let fansoType = document.querySelector("#ai-result-fan")




function thirteenOrphan(arr) {
    let necessaryTile = [{
        type: "char",
        value: 1
    }, {
        type: "char",
        value: 9
    }, {
        type: "bamboo",
        value: 1
    }, {
        type: "bamboo",
        value: 9
    }, {
        type: "dot",
        value: 1
    }, {
        type: "dot",
        value: 9
    }, {
        type: "wind",
        value: "east"
    }, {
        type: "wind",
        value: "south"
    }, {
        type: "wind",
        value: "west"
    }, {
        type: "wind",
        value: "north"
    }, {
        type: "dragon",
        value: "red"
    }, {
        type: "dragon",
        value: "white"
    }, {
        type: "dragon",
        value: "green"
    }]

    let checkTile = []
    let result2
    let result1
    for (let i = 0; i < arr.length; i++) {
        result2 = necessaryTile.some((tile) => {
            return tile.type === arr[i].type && tile.value === arr[i].value
        })
        if (necessaryTile.length > i) {
            result1 = arr.some((tile) => {
                return tile.type === necessaryTile[i].type && tile.value === necessaryTile[i].value
            })
        }

        checkTile.push(result1)
        checkTile.push(result2)
    }
    let allGood = checkTile.every((result) => {
        return result === true
    })
    if (allGood) {
        return {
            match: true,
            type: "十三么"
        }
    } else {
        return {
            match: false,
            type: "十三么"
        }
    }

}

// 全么九
function checkOneAndNine(arr, resultSet1) {
    let matchOneAndNine = true
    let checkThreeSet
    for (let k = 0; k < 4; k++) {
        checkThreeSet = resultSet1[k] === "same"
        if (!checkThreeSet || resultSet1[resultSet1.length - 1] !== "isPair") {
            matchOneAndNine = false
        }
    }
    for (let i = 0; i < arr.length; i += 3) {
        if (arr[i].value !== 1 && arr[i].value !== 9) {
            matchOneAndNine = false
        }
    }
    if (matchOneAndNine) {
        return {
            match: true,
            type: "全么九"
        }
    } else {
        return {
            match: false,
            type: "全么九"
        }
    }
}



// 大四喜
function bigFourJoys(arr, result2) {
    let result3
    let correct = true
    let wind = ["east", "south", "west", "north"]
    for (let i = 0; i < 4; i++) {
        result3 = arr.some((tile) => {
            return tile.value === wind[i]
        })
        if (!result3) {
            correct = false
        }
        if (result2[i] !== "same") {
            correct = false
        }
        if (result2[result2.length - 1] !== "isPair") {
            correct = false
        }

    }
    return {
        match: correct,
        type: "大四喜"
    }
}


// 小四喜
function tinyFourJoys(arr, result, result2) {
    let result3
    let correct = true
    let checkWind = []
    let wind = ["east", "south", "west", "north"]
    for (let i = 0; i < 4; i++) {
        result3 = arr.some((tile) => {
            return tile.value === wind[i]
        })
        checkWind.push(result3)

    }
    if (result2[result2.length - 1] !== "isPair" || result[result.length - 1][0].type !== "wind" || result[result.length - 1][1].type !== "wind") {
        correct = false
    }
    let result7 = checkWind.every((tileSet) => {
        return tileSet === true
    })
    let num = 0
    for (let k = 0; k < 4; k++) {
        if (result2[k] === "same" && result[k][0].type === "wind") {
            num++
        } else {
            let result5 = result2[k] === "same" || result2[k] === "serial"
            if (!result5) {
                correct = false
            }

        }
    }
    if (num !== 3 || !result7) {
        correct = false
    }
    return {
        match: correct,
        type: "小四喜"
    }
}

// 大三元

function bigThreeYuen(arr, result, result2) {
    let result3
    let correct = true
    let checkDragon = []
    let dragon = ["red", "white", "green", ""]
    for (let i = 0; i < dragon.length; i++) {
        result3 = arr.some((tile) => {
            return tile.value === dragon[i]
        })
        checkDragon.push(result3)

    }
    if (result2[result2.length - 1] !== "isPair" || result[result.length - 1][0].type === "dragon" || result[result.length - 1][1].type === "dragon") {
        correct = false
    }
    result3 = []
    for (let k = 0; k < 4; k++) {
        if (checkDragon[k]) {
            result3.push(result2[k] === "same")
        } else {
            let result5 = result2[k] === "same" || result2[k] === "serial"
            if (!result5) {
                correct = false
            }
        }
    }
    let result4 = result3.every((dragon) => {
        return dragon === true
    })
    let result6 = result3.filter((dragon) => {
        return dragon === true
    })
    if (!result4 || result6.length !== 3) {
        correct = false
    }
    return {
        match: correct,
        type: "大三元"
    }

}
// 字一色
function allOneWord(arr, result2) {
    let result3 = result2.filter((tileSet) => {
        return tileSet === "same"
    })
    let correct = true

    if (result2[result2.length - 1] !== "isPair") {
        correct = false
    }

    let result5 = arr.every((tile) => {
        return typeof (tile.value) !== "number"
    })

    if (result3.length !== 4 || !result5) {
        correct = false
    }
    return {
        match: correct,
        type: "字一色"
    }
}

// 清一色 10番
function checkIfSameType10(arr) {
    let theType = arr[0].type
    let sameType = arr.every((tile) => {
        return tile.type === theType
    })

    if (sameType) {
        let set1 = threeTileSet(arr.slice(0, 3))
        let set2 = threeTileSet(arr.slice(3, 6))
        let set3 = threeTileSet(arr.slice(6, 9))
        let set4 = threeTileSet(arr.slice(9, 12))
        let set5 = twoTileSet(arr.slice(12, 14))
        let correct = true
        let result = [set1, set2, set3, set4]
        if (set5 === "wrong") {
            correct = false
        }
        result1 = result.some((tileSet) => {
            return tileSet === "wrong"
        })
        result2 = result.some((tileSet) => {
            return tileSet === "serial"
        })
        if (result1 || result2 || !correct) {
            correct = false
        }
        return {
            match: correct,
            type: "清一色 對對胡"
        }
    } else {
        return {
            match: false,
            type: "清一色 對對胡"
        }
    }

}



// 清一色 7番
function checkIfSameType7(arr) {
    let theType = arr[0].type
    let sameType = arr.every((tile) => {
        return tile.type === theType
    })
    if (sameType) {
        let set1 = threeTileSet(arr.slice(0, 3))
        let set2 = threeTileSet(arr.slice(3, 6))
        let set3 = threeTileSet(arr.slice(6, 9))
        let set4 = threeTileSet(arr.slice(9, 12))
        let set5 = twoTileSet(arr.slice(12, 14))
        let result = [set1, set2, set3, set4, set5]
        result1 = result.some((tileSet) => {
            return tileSet === "wrong"
        })
        if (result1) {
            return {
                match: false,
                type: "清一色 7番"
            }
        }
        return {
            match: true,
            type: "清一色 7番"
        }
    } else {
        return {
            match: false,
            type: "清一色 7番"
        }
    }

}

// 小三元
function tinyThreeYuen(arr, result, result2) {
    let result3
    let correct = true
    let checkDragon = []
    let dragon = ["red", "white", "green", ""]
    for (let i = 0; i < dragon.length; i++) {
        result3 = arr.some((tile) => {
            return tile.value === dragon[i]
        })
        checkDragon.push(result3)

    }
    if (result2[result2.length - 1] !== "isPair" || result[result.length - 1][0].type !== "dragon" || result[result.length - 1][1].type !== "dragon") {
        correct = false
    }
    result3 = []
    for (let k = 0; k < 3; k++) {
        result3.push(checkDragon[k])
    }
    let result4 = result3.every((tileSet) => {
        return tileSet === true
    })
    let result5 = result2.some((tileSet) => {
        return tileSet === "wrong"
    })
    if (!result4 || result5) {
        correct = false
    }
    let num = 0
    for (let i = 0; i < 4; i++) {
        if (result2[i] === "same" && result[i][0].type === "dragon") {
            num++
        }
    }

    if (num !== 2) {
        correct = false
    }
    return {
        match: correct,
        type: "小三元"
    }

}


// 對對胡
function matchMatchWu(result2) {
    let result3 = [...result2]
    let result4 = result3.pop()
    let result5 = result3.every((tileSet) => {
        return tileSet === "same"
    })
    if (result4 !== "isPair" || !result5) {
        return {
            match: false,
            type: "對對胡"
        }
    }
    return {
        match: true,
        type: "對對胡"
    }
}



// For more than 14 tiles set
// 十八羅漢
function tile18Set(result2) {
    let correct = true
    if (result2[result2.length - 1] !== "isPair" || result2.length !== 5) {
        correct = false
    }
    return {
        match: correct,
        type: "十八羅漢"
    }
}
// 三槓子 10番
function tile17Set10Fan(arr, result2) {
    try {
        let correct = true
        let num = 0
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same") {
                num++
            }
        }
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 1 || result3) {
            correct = false
        }
        return {
            match: correct,
            type: "三槓子 爆番"
        }
    } catch (e) {
        return {
            match: false,
            type: "三槓子 爆番"
        }
    }

}

// 三槓子 7番
function tile17Set7Fan(arr, result2) {
    try {
        let correct = true
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet) => {
            return tileSet === "wrong"
        })
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4) {
            correct = false
        }
        return {
            match: correct,
            type: "三槓子 7番"
        }
    } catch (e) {
        return {
            match: false,
            type: "三槓子 7番"
        }
    }

}
// 三槓子 3番
function tile17Set3Fan(result2) {
    try {
        let correct = true
        let num = 0
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same") {
                num++
            }
        }
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 1) {
            correct = false
        }
        return {
            match: correct,
            type: "三槓子 3番"
        }
    } catch (e) {
        return {
            match: false,
            type: "三槓子 3番"
        }
    }

}

// 二槓子 10番
function tile16Set10Fan(arr, result2) {
    try {
        let correct = true
        let num = 0
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same") {
                num++
            }
        }
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || num !== 2) {
            correct = false
        }
        return {
            match: correct,
            type: "二槓子 爆番"
        }
    } catch (e) {
        return {
            match: false,
            type: "二槓子 爆番"
        }
    }
}
// 二槓子 7番
function tile16Set7Fan(arr, result2) {
    try {
        let correct = true
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet) => {
            return tileSet === "wrong"
        })
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4) {
            correct = false
        }
        return {
            match: correct,
            type: "二槓子 7番"
        }
    } catch (e) {
        return {
            match: false,
            type: "二槓子 7番"
        }
    }
}
// 二槓子 3番
function tile16Set3Fan(result2) {
    try {
        let correct = true
        let num = 0
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same") {
                num++
            }
        }
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 2) {
            correct = false
        }
        return {
            match: correct,
            type: "二槓子 3番"
        }
    } catch (e) {
        return {
            match: false,
            type: "二槓子 3番"
        }
    }

}

// 一槓子 10番
function tile15Set10Fan(arr, result2) {
    try {
        let correct = true
        let num = 0
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same") {
                num++
            }
        }
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || num !== 3) {
            correct = false
        }
        return {
            match: correct,
            type: "一槓子 爆番"
        }
    } catch (e) {
        return {
            match: false,
            type: "一槓子 爆番"
        }
    }
}
// 一槓子 7番
function tile15Set7Fan(arr, result2) {
    try {
        let correct = true
        let result3 = arr.every((tile) => {
            return tile.type === arr[0].type
        })
        let result4 = result2.some((tileSet) => {
            return tileSet === "wrong"
        })
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || !result3 || result4) {
            correct = false
        }
        return {
            match: correct,
            type: "一槓子 7番"
        }
    } catch (e) {
        return {
            match: false,
            type: "一槓子 7番"
        }
    }
}

// 一槓子 3番
function tile15Set3Fan(result2) {
    try {
        let correct = true
        let num = 0
        for (let i = 0; i < result2.length; i++) {
            if (result2[i] === "same" || result2[i] === "FourIsSame") {
                num++
            }
        }
        let result4 = result2.some((tileSet) => {
            return tileSet === "wrong"
        })
        if (result2[result2.length - 1] !== "isPair" || result2.length !== 5 || num !== 3 || result4) {
            correct = false
        }
        return {
            match: correct,
            type: "一槓子 3番"
        }
    } catch (e) {
        return {
            match: false,
            type: "一槓子 3番"
        }
    }

}
// 混一色
function blendOneColor(allTilesArr, allGroupedArr) {
    let isThreeTypeArr = []
    let isAnotherTypeArr = []
    let threeType = ["dot", "bamboo", "char"]
    let anotherType = ["wind", "dragon"]
    for (let i = 0; i < threeType.length; i++) {
        let isThreeType = allTilesArr.some((tile) => {
            return tile.type === threeType[i]
        })
        if (i < anotherType.length) {
            let isAnotherType = allTilesArr.some((tile) => {
                return tile.type === anotherType[i]
            })
            isAnotherTypeArr.push(isAnotherType)
        }
        isThreeTypeArr.push(isThreeType)
    }
    let hasTrue = isAnotherTypeArr.some((result) => {
        return result === true
    })
    let result5 = isThreeTypeArr.filter((result) => {
        return result === true
    })
    let hasWrong = allGroupedArr.some((tileSet) => {
        return tileSet === "wrong"
    })
    if (result5.length !== 1 || !hasTrue || hasWrong) {
        return {
            match: false
        }
    }
    return {
        match: true,
        type: "混一色"
    }
}


// 平胡
function plainWu(result2) {
    try {
        for (let i = 0; i < 4; i++) {
            if (result2[i] !== "serial") {
                correct = false
            }
        }
        let correct = true
        if (result2[result2.length - 1] !== "isPair") {
            correct = false
        }
        return {
            match: correct,
            type: "平胡"
        }
    } catch (e) {
        return {
            match: false,
            type: "平胡"
        }
    }
}


// 花么
function flowerYu(arr, result2) {
    let correct = true
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].value === i && i !== 1 && i !== 9) {
            correct = false
        }
        arr[i]
    }
    let result3 = result2.some((tileSet) => {
        return tileSet === "wrong"
    })
    let result4 = result2.some((tileSet) => {
        return tileSet === "serial"
    })
    if (result3 || result4) {
        correct = false
    }
    return {
        match: correct,
        type: "花么"
    }
}


// 中發白

function middleFanWhite(result, result2) {

    let correct = false
    let matchArr = []
    for (let i = 0; i < result2.length; i++) {
        if (result2[i] === "same" && result[i][0].type === "dragon") {
            correct = true
            matchArr.push("dragon")
        }
    }
    let result3 = result2.some((tileSet) => {
        return tileSet === "wrong"
    })
    if (result3) {
        correct = false
    }

    return {
        match: correct,
        matchNum: matchArr.length,
        type: "碰番子"
    }
}

function chickenWu(result2) {
    let correct = true
    let result3 = result2.some((setTile) => {
        return setTile === "wrong"
    })
    if (result3) {
        correct = false
    }
    return {
        match: correct,
        type: "雞糊"
    }
}

// end of hard code
function filterArrLength(arr) {
    aiResult.style.visibility = "visible"
    let fanso = "爆番"
    aiFansoDisplay.innerHTML = 0
    fansoType.innerHTML = ''
    try {
        let result = categorizeToArr(arr)
        let result2 = checkSetFunc(result)

        if (arr.length === 18) {
            let tile18SetVar = tile18Set(result2)
            if (tile18SetVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = tile18SetVar.type
                return
            }
            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        } else if (arr.length === 17) {
            let tile17Set10FanVar = tile17Set10Fan(arr, result2)
            if (tile17Set10FanVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = tile17Set10FanVar.type
                return
            }
            let tile17Set7FanVar = tile17Set7Fan(arr, result2)
            if (tile17Set7FanVar.match) {
                aiFansoDisplay.innerHTML = 7
                fansoType.innerHTML = tile17Set7FanVar.type
                return
            }
            let tile17Set3FanVar = tile17Set3Fan(result2)
            if (tile17Set3FanVar.match) {
                aiFansoDisplay.innerHTML = 3
                fansoType.innerHTML = tile17Set3FanVar.type
                return
            }
            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        } else if (arr.length === 16) {
            let tile16Set10FanVar = tile16Set10Fan(arr, result2)
            if (tile16Set10FanVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = tile16Set10FanVar.type
                return
            }
            let tile16Set7FanVar = tile16Set7Fan(arr, result2)
            if (tile16Set7FanVar.match) {
                aiFansoDisplay.innerHTML = 7
                fansoType.innerHTML = tile16Set7FanVar.type
                return
            }
            let tile16Set3FanVar = tile16Set3Fan(result2)
            if (tile16Set3FanVar.match) {
                aiFansoDisplay.innerHTML = 3
                fansoType.innerHTML = tile16Set3FanVar.type
                return
            }
            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        } else if (arr.length === 15) {
            let tile15Set10FanVar = tile15Set10Fan(arr, result2)
            if (tile15Set10FanVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = tile15Set10FanVar.type
                return
            }
            let tile15Set7FanVar = tile15Set7Fan(arr, result2)
            if (tile15Set7FanVar.match) {
                aiFansoDisplay.innerHTML = 7
                fansoType.innerHTML = tile15Set7FanVar.type
                return
            }
            let tile15Set3FanVar = tile15Set3Fan(result2)
            if (tile15Set3FanVar.match) {
                aiFansoDisplay.innerHTML = 3
                fansoType.innerHTML = tile15Set3FanVar.type
                return
            }
            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        } else if (arr.length === 14) {

            let thirteenOrphanVar = thirteenOrphan(arr)
            if (thirteenOrphanVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = thirteenOrphanVar.type
                return
            }

            let checkOneAndNineVar = checkOneAndNine(arr, result2)
            if (checkOneAndNineVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = checkOneAndNineVar.type
                return
            }
            let bigFourJoysVar = bigFourJoys(arr, result2)
            if (bigFourJoysVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = bigFourJoysVar.type
                return
            }
            let tinyFourJoysVar = tinyFourJoys(arr, result, result2)
            if (tinyFourJoysVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = tinyFourJoysVar.type
                return
            }
            let bigThreeYuenVar = bigThreeYuen(arr, result, result2)
            if (bigThreeYuenVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = bigThreeYuenVar.type
                return
            }

            let allOneWordVar = allOneWord(arr, result2)
            if (allOneWordVar.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = allOneWordVar.type
                return
            }

            let checkIfSameType10Var = checkIfSameType10(arr)
            if (checkIfSameType10Var.match) {
                aiFansoDisplay.innerHTML = fanso
                fansoType.innerHTML = checkIfSameType10Var.type
                return
            }

            let checkIfSameType7Var = checkIfSameType7(arr)
            if (checkIfSameType7Var.match) {
                aiFansoDisplay.innerHTML = 7
                fansoType.innerHTML = checkIfSameType7Var.type
                return
            }
            let blendOneColorVar = blendOneColor(arr, result2)
            if (blendOneColorVar.match) {
                aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 3
                fansoType.innerHTML = blendOneColorVar.type
            }
            let tinyThreeYuenVar = tinyThreeYuen(arr, result, result2)
            if (tinyThreeYuenVar.match) {
                aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 5
                fansoType.innerHTML = tinyThreeYuenVar.type

            }
            let matchMatchWuVar = matchMatchWu(result2)
            if (matchMatchWuVar.match) {
                aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 3
                fansoType.innerHTML = fansoType.innerHTML + " " + matchMatchWuVar.type
            }
            let plainWuVar = plainWu(result2)
            if (plainWuVar.match) {
                aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 1
                fansoType.innerHTML = fansoType.innerHTML + " " + plainWuVar.type
            }
            let middleFanWhiteVar = middleFanWhite(result, result2)
            if (middleFanWhiteVar.match) {
                if (middleFanWhiteVar.matchNum === 1) {
                    aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 1
                    fansoType.innerHTML = fansoType.innerHTML + " " + middleFanWhiteVar.type
                } else if (middleFanWhiteVar.matchNum === 2) {
                    aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 2
                    fansoType.innerHTML = fansoType.innerHTML + " " + middleFanWhiteVar.type
                } else if (middleFanWhiteVar.matchNum === 3) {
                    aiFansoDisplay.innerHTML = parseInt(aiFansoDisplay.innerHTML) + 3
                    fansoType.innerHTML = fansoType.innerHTML + " " + middleFanWhiteVar.type
                }
                return
            }

            if (middleFanWhiteVar.match || plainWuVar.match || matchMatchWuVar.match || tinyThreeYuenVar.match) {
                return
            }
            let chickenWuVar = chickenWu(result2)
            if (chickenWuVar.match) {
                aiFansoDisplay.innerHTML = 0
                fansoType.innerHTML = chickenWuVar.type
                return
            }

            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        } else {
            fansoType.innerHTML = "詐糊"
            aiResult.style.visibility = "hidden"
            return
        }
    } catch (e) {
        fansoType.innerHTML = "詐糊"
        aiResult.style.visibility = "hidden"
    }
}

function categorizeToArr(resultArr) {
    let setTile = []
    let filteredArr
    for (let i = 0; i < resultArr.length; i++) {
        filteredArr = resultArr.filter((tile) => {
            return tile.type === resultArr[i].type && tile.value === resultArr[i].value
        })
        if (resultArr.length > 14) {

            let checkIfPushedFour = setTile.some((tileArr) => {
                return tileArr.includes(filteredArr[0])
            })

            if (filteredArr.length === 4 && !checkIfPushedFour) {
                setTile.push(filteredArr)
            } else if (filteredArr.length === 5 && !checkIfPushedFour) {
                throw Error
            }
        }
    }
    if (resultArr.length === 18) {
        let eyeSet = resultArr.slice(resultArr.length - 2, resultArr.length)
        setTile.push(eyeSet)
        return setTile
    } else if (resultArr.length > 14) {
        let arrLength = resultArr.length
        let eyeSet = resultArr.slice(resultArr.length - 2, resultArr.length)
        let copyResultArr = [...resultArr]
        copyResultArr.pop()
        copyResultArr.pop()
        for (let i = 0; i < 4; i++) {
            copyResultArr = copyResultArr.filter((tileSet) => {
                return tileSet != setTile[0][i]
            })
            if (arrLength > 15) {
                copyResultArr = copyResultArr.filter((tileSet) => {
                    return tileSet != setTile[1][i]
                })
            }

            if (arrLength > 16) {
                copyResultArr = copyResultArr.filter((tileSet) => {
                    return tileSet != setTile[2][i]
                })
            }

        }
        if (arrLength === 17) {
            copyResultArr = copyResultArr.splice(0, 3)
        }
        if (arrLength === 16) {
            let threeSetArr1 = copyResultArr.splice(0, 3)
            setTile.push(threeSetArr1)
        }
        if (arrLength === 15) {
            let threeSetArr1 = copyResultArr.splice(0, 3)
            let threeSetArr2 = copyResultArr.splice(0, 3)
            setTile.push(threeSetArr1)
            setTile.push(threeSetArr2)
        }
        setTile.push(copyResultArr)
        setTile.push(eyeSet)
        return setTile
    }
    let set1 = resultArr.slice(0, 3)
    let set2 = resultArr.slice(3, 6)
    let set3 = resultArr.slice(6, 9)
    let set4 = resultArr.slice(9, 12)
    setTile.push(set1)
    setTile.push(set2)
    setTile.push(set3)
    setTile.push(set4)
    setTile.push(resultArr.slice(12, 14))
    return setTile
}


function checkSetFunc(arr) {
    let cateArr = []
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].length === 4) {
            cateArr.push(fourTileSet(arr[i]))
        } else if (arr[i].length === 3) {
            cateArr.push(threeTileSet(arr[i]))
        } else if (arr[i].length === 2) {
            cateArr.push(twoTileSet(arr[i]))
        }
    }
    return cateArr
}


function fourTileSet(fourArr) {
    let checkType = fourArr[0].type === fourArr[1].type && fourArr[1].type === fourArr[2].type && fourArr[2].type === fourArr[3].type
    let checkValue = fourArr[0].value === fourArr[1].value && fourArr[1].value === fourArr[2].value && fourArr[2].value === fourArr[3].value
    if (checkValue && checkType) {
        return "FourIsSame"
    } else {
        return "wrong"
    }
}

function threeTileSet(threeArr) {
    let isThreeSameType = threeArr[0].type === threeArr[1].type && threeArr[1].type === threeArr[2].type
    if (!isThreeSameType) {
        return "wrong"
    }
    let sameThreeTile = threeArr[0].value === threeArr[1].value && threeArr[1].value === threeArr[2].value
    if (sameThreeTile) {
        return "same"
    }
    threeArr.sort((a, b) => a.value - b.value)
    let serialThreeTile = threeArr[1].value === threeArr[0].value + 1 && threeArr[2].value === threeArr[0].value + 2
    if (serialThreeTile) {
        return "serial"
    }
    return "wrong"


}

function twoTileSet(eyeArr) {
    let checkIfPair = eyeArr[0].type === eyeArr[1].type && eyeArr[0].value === eyeArr[1].value
    if (checkIfPair) {
        return "isPair"
    } else {
        return "wrong"
    }
}